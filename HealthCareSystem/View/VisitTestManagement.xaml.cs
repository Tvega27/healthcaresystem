﻿using Windows.UI.Xaml.Controls;
using HealthCareSystem.ViewModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HealthCareSystem.View
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VisitTestManagement : Page
    {
        private readonly VisitTestViewModel _viewModel;

        public VisitTestManagement()
        {
            InitializeComponent();
            _viewModel = DataContext as VisitTestViewModel;
        }
    }
}