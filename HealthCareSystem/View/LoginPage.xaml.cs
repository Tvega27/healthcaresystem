﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using HealthCareSystem.ViewModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HealthCareSystem.View
{
    /// <summary>
    ///     A frame for the Login Page.
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.Page" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    /// <inheritdoc />
    public sealed partial class LoginPage
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LoginPage" /> class.
        /// </summary>
        public LoginPage()
        {
            InitializeComponent();
            ViewModel = new LoginViewModel();
        }

        public LoginViewModel ViewModel { get; set; }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null) ViewModel.Password = ((PasswordBox) sender).Password;
        }
    }
}