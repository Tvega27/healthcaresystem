﻿using Windows.UI.Xaml.Controls;
using HealthCareSystem.ViewModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HealthCareSystem.View
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PatientManagementControl : Page
    {
        private AddPatientViewModel _viewModel;

        public PatientManagementControl()
        {
            _viewModel = new AddPatientViewModel();
            InitializeComponent();
        }
    }
}