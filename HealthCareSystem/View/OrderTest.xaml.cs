﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using HealthCareSystem.Model;
using HealthCareSystem.ViewModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HealthCareSystem.View
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class OrderTest : Page
    {
        private readonly OrderTestViewModel _viewModel;

        public OrderTest()
        {
            InitializeComponent();
            _viewModel = new OrderTestViewModel();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _viewModel.Visit = e.Parameter as Visit ??
                               throw new InvalidOperationException("There has been a fatal error");
            DataContext = _viewModel;
        }

        private void Done_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }
    }
}