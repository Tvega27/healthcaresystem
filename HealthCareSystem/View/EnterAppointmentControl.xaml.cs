﻿using System;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace HealthCareSystem.View
{
    /// <summary>
    ///     A control for entering appointments.
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    public sealed partial class EnterAppointmentControl
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EnterAppointmentControl" /> class.
        /// </summary>
        public EnterAppointmentControl()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Gets the name of the patient.
        /// </summary>
        /// <returns></returns>
        public string GetPatientName()
        {
            return patientTextBox.Text;
        }


        /// <summary>
        ///     Gets the date.
        /// </summary>
        /// <returns></returns>
        public DateTime GetDate()
        {
            return calendarPicker.Date.Value.Date;
        }

        /// <summary>
        ///     Gets the time.
        /// </summary>
        /// <returns></returns>
        public TimeSpan GetTime()
        {
            return timePicker.Time;
        }

        /// <summary>
        ///     Gets the reasons for the appointment.
        /// </summary>
        /// <returns></returns>
        public string GetReasons()
        {
            return reasonsTextBox.Text;
        }
    }
}