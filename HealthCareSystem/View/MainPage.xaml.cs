﻿using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using HealthCareSystem.Model;
using HealthCareSystem.ViewModel;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HealthCareSystem.View
{
    /// <summary>
    ///     The Main page of the documentation
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.Page" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    /// <inheritdoc />
    public sealed partial class MainPage
    {
        private readonly MainPageViewModel _viewModel;

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:HealthCareSystem.View.MainPage" /> class.
        /// </summary>
        /// <inheritdoc />
        public MainPage()
        {
            InitializeComponent();
            _viewModel = DataContext as MainPageViewModel;
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {
        }

        private void LoggedInUserDisplay_SelectionChanged(object sender, RoutedEventArgs e)
        {
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var ticket = e.Parameter as LoginTicket ??
                         throw new InvalidOperationException("There has been a fatal error");
            LoggedInUserDisplay.Text = ticket.UserName;
            if (ticket.IsAdmin)
                mainPivot.Items.Remove(mainPivot.Items.Single(p => ((PivotItem) p).Name == "AdminPivot"));
        }

        private void patientTextBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
        }
    }
}