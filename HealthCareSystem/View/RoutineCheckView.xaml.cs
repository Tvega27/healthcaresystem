﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using HealthCareSystem.Model;
using HealthCareSystem.ViewModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HealthCareSystem.View
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RoutineCheckView : Page
    {
        private readonly VisitViewModel _viewModel;

        public RoutineCheckView()
        {
            InitializeComponent();
            _viewModel = DataContext as VisitViewModel;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DataContext = _viewModel;
            _viewModel.Visit = e.Parameter as Visit ??
                               throw new InvalidOperationException("There has been a fatal error");
            _viewModel.UpdateRoutineCheck();
            backBtn.IsEnabled = Frame.CanGoBack;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            On_BackRequested();
        }

        // Handles system-level BackRequested events and page-level back button Click events
        private bool On_BackRequested()
        {
            if (Frame.CanGoBack)
            {
                Frame.GoBack();
                return true;
            }

            return false;
        }
    }
}