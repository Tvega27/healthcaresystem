﻿using System;
using System.Collections.Generic;
using HealthCareSystem.DAL;
using HealthCareSystem.Model;

namespace HealthCareSystem.Utility
{
    public class PatientSearches
    {
        private readonly PatientDal _dal;

        public PatientSearches()
        {
            _dal = new PatientDal();
        }

        public List<Patient> FindPatients(PatientSearch search)
        {
            var foundPatients = new List<Patient>();

            if (search.Birthdate != null && search.LastName != null && search.Ssn != null)
                foundPatients =
                    _dal.QueryPatientsByDateSsnAndName((DateTime) search.Birthdate, search.Ssn, search.LastName);
            else if (search.Birthdate == null && search.LastName != null && search.Ssn != null)
                foundPatients = _dal.QueryPatientsByNameAndSsn(search.LastName, search.Ssn);
            else if (search.Birthdate != null && search.LastName != null && search.Ssn == null)
                foundPatients = _dal.QueryPatientsByNameAndDate(search.LastName, (DateTime) search.Birthdate);
            else if (search.Birthdate != null && search.LastName == null && search.Ssn != null)
                foundPatients = _dal.QueryPatientsByDateAndSsn((DateTime) search.Birthdate, search.Ssn);
            else if (search.Birthdate == null && search.LastName == null && search.Ssn != null)
                foundPatients = _dal.QueryPatientsBySsn(search.Ssn);
            else if (search.Birthdate == null && search.LastName != null && search.Ssn == null)
                foundPatients = _dal.QueryPatientsByLastName(search.LastName);
            else if (search.Birthdate != null && search.LastName == null && search.Ssn == null)
                foundPatients = _dal.QueryPatientsByBirthDate((DateTime) search.Birthdate);

            return foundPatients;
        }

        public static List<Patient> FindPatientAppointments(PatientSearch search)
        {
            if (search.Birthdate != null && search.LastName != null && search.Ssn != null)
            {
                // all three
            }
            else if (search.Birthdate == null && search.LastName != null && search.Ssn != null)
            {
                // ln and ssn
            }
            else if (search.Birthdate != null && search.LastName != null && search.Ssn == null)
            {
                // ln and birthday
            }
            else if (search.Birthdate != null && search.LastName == null && search.Ssn != null)
            {
                // ln and birthday
            }
            else if (search.Birthdate == null && search.LastName == null && search.Ssn != null)
            {
                // ssn
            }
            else if (search.Birthdate == null && search.LastName != null && search.Ssn == null)
            {
                // last name
            }
            else if (search.Birthdate != null && search.LastName == null && search.Ssn == null)
            {
                // birthday
            }

            return null;
        }
    }
}