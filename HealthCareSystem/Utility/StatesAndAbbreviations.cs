﻿using System;
using System.Collections.Generic;

namespace HealthCareSystem.Utility
{
    public static class StatesAndAbbreviations
    {
        public static string GetAbbreviationFromStateName(string stateName)
        {
            switch (stateName.ToUpper())
            {
                case "ALABAMA":
                    return "AL";

                case "ALASKA":
                    return "AK";

                case "AMERICAN SAMOA":
                    return "AS";

                case "ARIZONA":
                    return "AZ";

                case "ARKANSAS":
                    return "AR";

                case "CALIFORNIA":
                    return "CA";

                case "COLORADO":
                    return "CO";

                case "CONNECTICUT":
                    return "CT";

                case "DELAWARE":
                    return "DE";

                case "DISTRICT OF COLUMBIA":
                    return "DC";

                case "FEDERATED STATES OF MICRONESIA":
                    return "FM";

                case "FLORIDA":
                    return "FL";

                case "GEORGIA":
                    return "GA";

                case "GUAM":
                    return "GU";

                case "HAWAII":
                    return "HI";

                case "IDAHO":
                    return "ID";

                case "ILLINOIS":
                    return "IL";

                case "INDIANA":
                    return "IN";

                case "IOWA":
                    return "IA";

                case "KANSAS":
                    return "KS";

                case "KENTUCKY":
                    return "KY";

                case "LOUISIANA":
                    return "LA";

                case "MAINE":
                    return "ME";

                case "MARSHALL ISLANDS":
                    return "MH";

                case "MARYLAND":
                    return "MD";

                case "MASSACHUSETTS":
                    return "MA";

                case "MICHIGAN":
                    return "MI";

                case "MINNESOTA":
                    return "MN";

                case "MISSISSIPPI":
                    return "MS";

                case "MISSOURI":
                    return "MO";

                case "MONTANA":
                    return "MT";

                case "NEBRASKA":
                    return "NE";

                case "NEVADA":
                    return "NV";

                case "NEW HAMPSHIRE":
                    return "NH";

                case "NEW JERSEY":
                    return "NJ";

                case "NEW MEXICO":
                    return "NM";

                case "NEW YORK":
                    return "NY";

                case "NORTH CAROLINA":
                    return "NC";

                case "NORTH DAKOTA":
                    return "ND";

                case "NORTHERN MARIANA ISLANDS":
                    return "MP";

                case "OHIO":
                    return "OH";

                case "OKLAHOMA":
                    return "OK";

                case "OREGON":
                    return "OR";

                case "PALAU":
                    return "PW";

                case "PENNSYLVANIA":
                    return "PA";

                case "PUERTO RICO":
                    return "PR";

                case "RHODE ISLAND":
                    return "RI";

                case "SOUTH CAROLINA":
                    return "SC";

                case "SOUTH DAKOTA":
                    return "SD";

                case "TENNESSEE":
                    return "TN";

                case "TEXAS":
                    return "TX";

                case "UTAH":
                    return "UT";

                case "VERMONT":
                    return "VT";

                case "VIRGIN ISLANDS":
                    return "VI";

                case "VIRGINIA":
                    return "VA";

                case "WASHINGTON":
                    return "WA";

                case "WEST VIRGINIA":
                    return "WV";

                case "WISCONSIN":
                    return "WI";

                case "WYOMING":
                    return "WY";
            }

            throw new Exception("Not Available");
        }

        public static IList<string> GetStateNameList()
        {
            var stateList = new List<string>();
            stateList.Add("Alabama");
            stateList.Add("Alaska");
            stateList.Add("Arizona");
            stateList.Add("Arkansas");
            stateList.Add("California");
            stateList.Add("Colorado");
            stateList.Add("Connecticut");
            stateList.Add("Delaware");
            stateList.Add("District of Columbia");
            stateList.Add("Florida");
            stateList.Add("Georgia");
            stateList.Add("Hawaii");
            stateList.Add("Idaho");
            stateList.Add("Illinois");
            stateList.Add("Indiana");
            stateList.Add("Iowa");
            stateList.Add("Kansas");
            stateList.Add("Kentucky");
            stateList.Add("Louisiana");
            stateList.Add("Maine");
            stateList.Add("Maryland");
            stateList.Add("Massachusetts");
            stateList.Add("Michigan");
            stateList.Add("Minnesota");
            stateList.Add("Mississippi");
            stateList.Add("Missouri");
            stateList.Add("Montana");
            stateList.Add("Nebraska");
            stateList.Add("Nevada");
            stateList.Add("New Hampshire");
            stateList.Add("New Jersey");
            stateList.Add("New Mexico");
            stateList.Add("New York");
            stateList.Add("North Carolina");
            stateList.Add("North Dakota");
            stateList.Add("Ohio");
            stateList.Add("Oklahoma");
            stateList.Add("Oregon");
            stateList.Add("Pennsylvania");
            stateList.Add("Rhode Island");
            stateList.Add("South Carolina");
            stateList.Add("South Dakota");
            stateList.Add("Tennessee");
            stateList.Add("Texas");
            stateList.Add("Utah");
            stateList.Add("Vermont");
            stateList.Add("Virginia");
            stateList.Add("Washington");
            stateList.Add("West Virginia");
            stateList.Add("Wisconsin");
            stateList.Add("Wyoming");
            return stateList;
        }
    }
}