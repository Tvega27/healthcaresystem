﻿namespace HealthCareSystem.Model
{
    public class Nurse : Person
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Nurse" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="userId">The user identifier.</param>
        public Nurse(int id, int userId)
        {
            Id = id;
            UserId = userId;
        }

        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the user identifier.
        /// </summary>
        /// <value>
        ///     The user identifier.
        /// </value>
        public int UserId { get; set; }
    }
}