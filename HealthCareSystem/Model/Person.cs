﻿using System;

namespace HealthCareSystem.Model
{
    public class Person
    {
        public Person()
        {
            Ssn = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
            Sex = string.Empty;
            Birthdate = DateTime.MinValue;
            AddressId = 0;
            PhoneNumber = string.Empty;
        }

        public Person(string ssn, string fname, string lname, string sex, DateTime birthdate, int addressId,
            string phoneNumber)
        {
            Ssn = ssn;
            FirstName = fname;
            LastName = lname;
            Sex = sex;
            Birthdate = birthdate;
            AddressId = addressId;
            PhoneNumber = phoneNumber;
        }

        public string Ssn { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Sex { get; set; }

        public DateTime Birthdate { get; set; }

        public int AddressId { get; set; }

        public string PhoneNumber { get; set; }
    }
}