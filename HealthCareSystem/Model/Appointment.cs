﻿using System;

namespace HealthCareSystem.Model
{
    /// <summary>
    ///     Object representing an appointment
    /// </summary>
    public class Appointment
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Appointment" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="patient">The patient.</param>
        /// <param name="doctor">The doctor.</param>
        /// <param name="date">The date.</param>
        /// <param name="reasons">The reasons.</param>
        /// <param name="completed">if set to <c>true</c> [completed].</param>
        public Appointment(int id, Patient patient, Doctor doctor, DateTime date, string reasons, bool completed)
        {
            AppointmentId = id;
            Patient = patient;
            Doctor = doctor;
            Date = date;

            Reasons = reasons;
            Completed = completed;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Appointment" /> class.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <param name="doctor">The doctor.</param>
        /// <param name="date">The date.</param>
        /// <param name="reasons">The reasons.</param>
        /// <param name="completed">if set to <c>true</c> [completed].</param>
        public Appointment(Patient patient, Doctor doctor, DateTime date, string reasons, bool completed)
        {
            AppointmentId = 0;
            Patient = patient;
            Doctor = doctor;
            Date = date;

            Reasons = reasons;
            Completed = completed;
        }

        public int AppointmentId { get; set; }

        /// <summary>
        ///     Gets or sets the patient.
        /// </summary>
        /// <value>
        ///     The patient.
        /// </value>
        public Patient Patient { get; set; }

        /// <summary>
        ///     Gets or sets the doctor.
        /// </summary>
        /// <value>
        ///     The doctor.
        /// </value>
        public Doctor Doctor { get; set; }

        /// <summary>
        ///     Gets or sets the date.
        /// </summary>
        /// <value>
        ///     The date.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        ///     Gets or sets the time.
        /// </summary>
        /// <value>
        ///     The time.
        /// </value>
        public TimeSpan Time { get; set; }

        /// <summary>
        ///     Gets or sets the reasons.
        /// </summary>
        /// <value>
        ///     The reasons.
        /// </value>
        public string Reasons { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this <see cref="Appointment" /> is completed.
        /// </summary>
        /// <value>
        ///     <c>true</c> if completed; otherwise, <c>false</c>.
        /// </value>
        public bool Completed { get; set; }

        /// <summary>
        ///     Gets the formatted appointment.
        /// </summary>
        /// <value>
        ///     The formatted appointment.
        /// </value>
        public string FormattedAppointment => Date + " at " + Time + ": " + Patient.GetPatientIdAndName() +
                                              " with Dr. " + Doctor.GetDoctorFullName();

        /// <summary>
        ///     Sets the appointment time to date.
        /// </summary>
        public void SetAppointmentTimeToDate()
        {
            Date = Date + Time;
        }
    }
}