﻿using System;
using System.Collections.Generic;

namespace HealthCareSystem.Model
{
    /// <summary>
    ///     A Simple class for storing and autheticating a user
    /// </summary>
    public class DemoUserDb

    {
        private readonly Dictionary<string, string> _userDatabase;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DemoUserDb" /> class.
        /// </summary>
        public DemoUserDb()
        {
            _userDatabase = new Dictionary<string, string>
            {
                ["testuser"] = "password",
                ["testuser2"] = "password2"
            };
        }

        /// <summary>
        ///     Checks the login information of a user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="pass">The user's password.</param>
        /// <returns>Returns true if the user is stored by this class, false otherwise</returns>
        /// <exception cref="ArgumentException">
        ///     user is null - user
        ///     or
        ///     user is null - pass
        /// </exception>
        public bool CheckLogin(string user, string pass)
        {
            if (user == null) throw new ArgumentException("user is null", nameof(user));

            if (pass == null) throw new ArgumentException("user is null", nameof(pass));

            user = user.ToLower();
            return _userDatabase.ContainsKey(user) && _userDatabase[user].Equals(pass);
        }
    }
}