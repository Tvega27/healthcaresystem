﻿namespace HealthCareSystem.Model
{
    public class Visit
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Visit" /> class.
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        /// <param name="prognosis">The prognosis.</param>
        /// <param name="notes">The notes.</param>
        public Visit(Appointment appointment, string prognosis, string notes)
        {
            Appointment = appointment;
            Prognosis = prognosis;
            Notes = notes;
        }

        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the appointment.
        /// </summary>
        /// <value>
        ///     The appointment.
        /// </value>
        public Appointment Appointment { get; set; }

        /// <summary>
        ///     Gets or sets the prognosis.
        /// </summary>
        /// <value>
        ///     The prognosis.
        /// </value>
        public string Prognosis { get; set; }

        /// <summary>
        ///     Gets or sets the notes.
        /// </summary>
        /// <value>
        ///     The notes.
        /// </value>
        public string Notes { get; set; }

        /// <summary>
        ///     Gets or sets the diagnosis.
        /// </summary>
        /// <value>
        ///     The diagnosis.
        /// </value>
        public string Diagnosis { get; set; }
    }
}