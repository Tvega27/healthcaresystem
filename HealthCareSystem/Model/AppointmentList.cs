﻿using System;
using System.Collections.Generic;

namespace HealthCareSystem.Model
{
    /// <summary>
    ///     Object for storing a list of <see cref="Appointment" />
    /// </summary>
    public class AppointmentList
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AppointmentList" /> class.
        /// </summary>
        public AppointmentList()
        {
            Appointments = new List<Appointment>();
        }

        public List<Appointment> Appointments { get; set; }

        /// <summary>
        ///     Adds the appointment.
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        public void AddAppointment(Appointment appointment)
        {
            if (appointment == null) throw new ArgumentException("user is null", nameof(appointment));
            Appointments.Add(appointment);
        }
    }
}