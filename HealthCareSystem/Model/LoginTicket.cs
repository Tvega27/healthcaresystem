﻿namespace HealthCareSystem.Model
{
    public class LoginTicket
    {
        public LoginTicket(string userName, bool isAdmin)
        {
            UserName = userName;
            IsAdmin = isAdmin;
        }


        public bool IsAdmin { get; set; }
        public string UserName { get; set; }
    }
}