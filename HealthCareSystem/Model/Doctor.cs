﻿namespace HealthCareSystem.Model
{
    public class Doctor : Person
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Doctor" /> class.
        /// </summary>
        /// <param name="doctorId">The doctor identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="fname">The first name.</param>
        /// <param name="lname">The last name.</param>
        /// <param name="field">The field.</param>
        public Doctor(int doctorId, int userId, string ssn, string fname, string lname, string field)
        {
            DoctorId = doctorId;
            UserId = userId;
            Ssn = ssn;
            FirstName = fname;
            LastName = lname;
            Field = field;
        }

        /// <summary>
        ///     Gets or sets the doctor identifier.
        /// </summary>
        /// <value>
        ///     The doctor identifier.
        /// </value>
        public int DoctorId { get; set; }

        /// <summary>
        ///     Gets or sets the user identifier.
        /// </summary>
        /// <value>
        ///     The user identifier.
        /// </value>
        public int UserId { get; set; }

        /// <summary>
        ///     Gets or sets the field.
        /// </summary>
        /// <value>
        ///     The field.
        /// </value>
        public string Field { get; set; }

        /// <summary>
        ///     Gets the full name.
        /// </summary>
        /// <value>
        ///     The full name.
        /// </value>
        public string DoctorFullName => FirstName + ' ' + LastName;

        /// <summary>
        ///     Gets the full name of the doctor.
        /// </summary>
        /// <returns></returns>
        public string GetDoctorFullName()
        {
            return FirstName + " " + LastName;
        }
    }
}