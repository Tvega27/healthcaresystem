﻿using System.Collections.Generic;
using System.Linq;
using HealthCareSystem.DAL;

namespace HealthCareSystem.Model
{
    internal class PatientCache
    {
        public List<Patient> Cache;
        public PatientDal Dal;

        public PatientCache()
        {
            Dal = new PatientDal();
            Cache = Dal.GetAllPatients();
        }

        public List<Patient> NameContains(string stringToCheck)
        {
            return Cache.Where(item => Cache.Any(x =>
                item.PatientFullName.Contains(stringToCheck))) as List<Patient>;
        }
    }
}