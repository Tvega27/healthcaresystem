﻿namespace HealthCareSystem.Model
{
    public class Address
    {
        public Address()
        {
            Streetaddress = string.Empty;
            City = string.Empty;
            State = string.Empty;
            Zipcode = string.Empty;
        }

        public Address(string streetaddress, string city, string state, string zipcode)
        {
            Streetaddress = streetaddress;
            City = city;
            State = state;
            Zipcode = zipcode;
        }

        public Address(int addressId, string streetaddress, string city, string state, string zipcode)
        {
            AddressId = addressId;
            Streetaddress = streetaddress;
            City = city;
            State = state;
            Zipcode = zipcode;
        }

        #region Data members

        #endregion

        #region Properties

        public int AddressId { get; set; }

        public string Streetaddress { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zipcode { get; set; }

        #endregion
    }
}