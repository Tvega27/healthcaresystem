﻿using System;

namespace HealthCareSystem.Model
{
    public class PatientSearch
    {
        public string LastName { get; set; }
        public string Ssn { get; set; }
        public DateTime? Birthdate { get; set; }
    }
}