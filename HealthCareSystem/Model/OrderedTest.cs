﻿using System;

namespace HealthCareSystem.Model
{
    public class OrderedTest
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="OrderedTest" /> class.
        /// </summary>
        /// <param name="test">The test.</param>
        /// <param name="visitId">The visit identifier.</param>
        /// <param name="orderDate">The order date.</param>
        public OrderedTest(Test test, int visitId, DateTime orderDate)
        {
            Test = test;
            VisitId = visitId;
            OrderDate = orderDate;
        }

        /// <summary>
        ///     Gets or sets the test.
        /// </summary>
        /// <value>
        ///     The test.
        /// </value>
        public Test Test { get; set; }

        /// <summary>
        ///     Gets or sets the visit identifier.
        /// </summary>
        /// <value>
        ///     The visit identifier.
        /// </value>
        public int VisitId { get; set; }

        /// <summary>
        ///     Gets or sets the results.
        /// </summary>
        /// <value>
        ///     The results.
        /// </value>
        public string Results { get; set; }

        /// <summary>
        ///     Gets or sets the date.
        /// </summary>
        /// <value>
        ///     The date.
        /// </value>
        public DateTime OrderDate { get; set; }

        /// <summary>
        ///     Gets or sets the completed date.
        /// </summary>
        /// <value>
        ///     The completed date.
        /// </value>
        public DateTime CompletedDate { get; set; }
    }
}