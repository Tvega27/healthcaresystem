﻿namespace HealthCareSystem.Model
{
    public class RoutineCheck
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="RoutineCheck" /> class.
        /// </summary>
        public RoutineCheck()
        {
            Id = 0;
            VisitId = 0;
            BpSystolic = 0;
            BpDiastolic = 0;
            OxygenSaturation = 0;
            PulseBpm = 0;
            Height = 0;
            Weight = 0;
            BodyTemp = 0;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RoutineCheck" /> class.
        /// </summary>
        /// <param name="visitId">The visit identifier.</param>
        /// <param name="bpSystolic">The bp systolic.</param>
        /// <param name="bpDiastolic">The bp diastolic.</param>
        /// <param name="oxygenSaturation">The oxygen saturation.</param>
        /// <param name="pulseBpm">The pulse BPM.</param>
        /// <param name="height">The height.</param>
        /// <param name="weight">The weight.</param>
        /// <param name="bodyTemp">The body temporary.</param>
        public RoutineCheck(int visitId, int bpSystolic, int bpDiastolic, int oxygenSaturation, int pulseBpm,
            decimal height, decimal weight, decimal bodyTemp)
        {
            VisitId = visitId;
            BpSystolic = bpSystolic;
            BpDiastolic = bpDiastolic;
            OxygenSaturation = oxygenSaturation;
            PulseBpm = pulseBpm;
            Height = height;
            Weight = weight;
            BodyTemp = bodyTemp;
        }

        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public int Id { get; set; }

        public int VisitId { get; set; }

        /// <summary>
        ///     Gets or sets the bp systolic.
        /// </summary>
        /// <value>
        ///     The bp systolic.
        /// </value>
        public int BpSystolic { get; set; }

        /// <summary>
        ///     Gets or sets the bp diastolic.
        /// </summary>
        /// <value>
        ///     The bp diastolic.
        /// </value>
        public int BpDiastolic { get; set; }

        /// <summary>
        ///     Gets or sets the oxygen saturation.
        /// </summary>
        /// <value>
        ///     The oxygen saturation.
        /// </value>
        public int OxygenSaturation { get; set; }

        /// <summary>
        ///     Gets or sets the pulse BPM.
        /// </summary>
        /// <value>
        ///     The pulse BPM.
        /// </value>
        public int PulseBpm { get; set; }

        /// <summary>
        ///     Gets or sets the height.
        /// </summary>
        /// <value>
        ///     The height.
        /// </value>
        public decimal Height { get; set; }

        /// <summary>
        ///     Gets or sets the weight.
        /// </summary>
        /// <value>
        ///     The weight.
        /// </value>
        public decimal Weight { get; set; }

        /// <summary>
        ///     Gets or sets the body temporary.
        /// </summary>
        /// <value>
        ///     The body temporary.
        /// </value>
        public decimal BodyTemp { get; set; }

        /// <summary>
        ///     Gets or sets the nurse identifier.
        /// </summary>
        /// <value>
        ///     The nurse identifier.
        /// </value>
        public Nurse Nurse { get; set; }
    }
}