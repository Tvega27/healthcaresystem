﻿namespace HealthCareSystem.Model
{
    public class Test
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Test" /> class.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="name">The name.</param>
        public Test(string code, string name)
        {
            Code = code;
            Name = name;
        }

        /// <summary>
        ///     Gets or sets the code.
        /// </summary>
        /// <value>
        ///     The code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        ///     Gets the name and the code of the test.
        /// </summary>
        /// <value>
        ///     The name and the code of the test.
        /// </value>
        public string CodeAndName => Code + " : " + Name;
    }
}