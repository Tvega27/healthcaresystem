﻿using System;
using System.Collections.Generic;

namespace HealthCareSystem.Model
{
    /// <summary>
    ///     Object describing a paitent
    /// </summary>
    public class Patient
    {
        public string PhoneNumber;


        /// <summary>
        ///     Initializes a new instance of the <see cref="Patient" /> class.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="sex">The sex.</param>
        /// <param name="birthdate">The birthdate.</param>
        /// <param name="primaryDoctor">The primary doctor.</param>
        public Patient(int patientId, string ssn, string firstName, string lastName, string sex, DateTime birthdate,
            Address address, string phone, int primaryDoctor)
        {
            PatientId = patientId;
            FirstName = firstName;
            LastName = lastName;
            Sex = sex;
            Birthdate = birthdate;
            PrimaryDoctor = primaryDoctor;
            Conditions = new List<string>();
            Ssn = ssn;
            Address = address;
            PhoneNumber = phone;
        }

        /// <summary>
        ///     Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        ///     The patient identifier.
        /// </value>
        public int PatientId { get; set; }

        /// <summary>
        ///     Gets or sets the first name.
        /// </summary>
        /// <value>
        ///     The first name.
        /// </value>
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets the last name.
        /// </summary>
        /// <value>
        ///     The last name.
        /// </value>
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets the sex.
        /// </summary>
        /// <value>
        ///     The sex.
        /// </value>
        public string Sex { get; set; }

        /// <summary>
        ///     Gets or sets the birthdate.
        /// </summary>
        /// <value>
        ///     The birthdate.
        /// </value>
        public DateTime Birthdate { get; set; }

        /// <summary>
        ///     Gets or sets the primary doctor.
        /// </summary>
        /// <value>
        ///     The primary doctor.
        /// </value>
        public int PrimaryDoctor { get; set; }

        /// <summary>
        ///     Gets or sets the conditions.
        /// </summary>
        /// <value>
        ///     The conditions.
        /// </value>
        public List<string> Conditions { get; set; }

        /// <summary>
        ///     Gets or sets the SSN.
        /// </summary>
        /// <value>
        ///     The SSN.
        /// </value>
        public string Ssn { get; set; }

        /// <summary>
        ///     Gets or sets the address.
        /// </summary>
        /// <value>
        ///     The address.
        /// </value>
        public Address Address { get; set; }

        /// <summary>
        ///     Gets the full name.
        /// </summary>
        /// <value>
        ///     The full name.
        /// </value>
        public string PatientFullName => FirstName + ' ' + LastName;

        /// <summary>
        ///     Gets the name of the patient identifier and.
        /// </summary>
        /// <returns></returns>
        public string GetPatientIdAndName()
        {
            return "ID: " + PatientId + " Name: " + FirstName + " " + LastName;
        }
    }
}