﻿using System;
using System.Collections.Generic;
using HealthCareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    public class TestDal
    {
        /// <summary>
        ///     Gets the test.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        public Test GetTest(string code)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var selectQuery = "SELECT * " +
                                  "FROM test " +
                                  "WHERE code = @code";

                using (var cmd = new MySqlCommand(selectQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@code", code);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var codeOrdinal = reader.GetOrdinal("code");
                        var nameOrdinal = reader.GetOrdinal("tName");

                        while (reader.Read())
                        {
                            var testCode = reader[codeOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(codeOrdinal);

                            var testName = reader[nameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(nameOrdinal);

                            return new Test(testCode, testName);
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        ///     Gets all tests.
        /// </summary>
        /// <returns></returns>
        public List<Test> GetAllTests()
        {
            var tests = new List<Test>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var selectQuery = "SELECT * " +
                                  "FROM test";

                using (var cmd = new MySqlCommand(selectQuery, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var codeOrdinal = reader.GetOrdinal("code");
                        var nameOrdinal = reader.GetOrdinal("tName");

                        while (reader.Read())
                        {
                            var testCode = reader[codeOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(codeOrdinal);

                            var testName = reader[nameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(nameOrdinal);

                            tests.Add(new Test(testCode, testName));
                        }
                    }
                }
            }

            return tests;
        }

        public List<Test> GetTestsByName(string input)
        {
            var tests = new List<Test>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var selectQuery = "SELECT * " +
                                  "FROM test " +
                                  "WHERE tName Like @input";

                using (var cmd = new MySqlCommand(selectQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@input", "%" + input + "%");
                    using (var reader = cmd.ExecuteReader())
                    {
                        var codeOrdinal = reader.GetOrdinal("code");
                        var nameOrdinal = reader.GetOrdinal("tName");

                        while (reader.Read())
                        {
                            var testCode = reader[codeOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(codeOrdinal);

                            var testName = reader[nameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(nameOrdinal);

                            tests.Add(new Test(testCode, testName));
                        }
                    }
                }
            }

            return tests;
        }
    }
}