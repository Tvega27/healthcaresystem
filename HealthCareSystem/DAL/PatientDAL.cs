﻿using System;
using System.Collections.Generic;
using HealthCareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    public class PatientDal
    {
        /// <summary>
        ///     Gets all patients.
        /// </summary>
        /// <returns></returns>
        public List<Patient> GetAllPatients()
        {
            var patients = new List<Patient>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "SELECT person.*, address.*, patient.* FROM patient INNER JOIN person ON patient.pssn = person.ssn INNER JOIN address ON person.addressId = address.addressID";


                using (var cmd = new MySqlCommand(query, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("fname");
                        var pLNameOrdinal = reader.GetOrdinal("lname");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phone");
                        var aAddressIdOrdinal = reader.GetOrdinal("addressId");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aAddressId = reader[aAddressIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[aAddressIdOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];


                            var address = new Address(aAddressId, aStreetAddress, aCity, aState, aZipcode);

                            patients.Add(new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor));
                        }
                    }
                }
            }

            return patients;
        }

        /// <summary>
        ///     Gets the patient.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public Patient GetPatient(int patientId)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();

                var q = "SELECT * " +
                        "FROM patient, person, address " +
                        "WHERE patient.pssn = person.ssn " +
                        "AND person.addressId = address.addressID " +
                        "AND patientID = @patientId";

                using (var cmd = new MySqlCommand(q, conn))
                {
                    cmd.Parameters.AddWithValue("@patientId", patientId);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("fname");
                        var pLNameOrdinal = reader.GetOrdinal("lname");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phone");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];
                            var address = new Address(aStreetAddress, aCity, aState, aZipcode);

                            return new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor);
                        }
                    }
                }
            }

            return null;
        }

        private List<Patient> PatientQuery(string query)
        {
            var columnData = new List<Patient>();

            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                using (var cmd = new MySqlCommand(query, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("pFName");
                        var pLNameOrdinal = reader.GetOrdinal("pLName");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phoneNumber");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];

                            var address = new Address(aStreetAddress, aCity, aState, aZipcode);

                            columnData.Add(new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor));
                        }
                    }
                }

                conn.Close();
            }

            return columnData;
        }

        /// <summary>
        ///     Adds the patient.
        /// </summary>
        /// <param name="patient">The patient.</param>
        public void AddPatient(Patient patient)
        {
            var addressId = -1;

            #region TryInsertingAddress

            try
            {
                using (var conn = DbConnection.GetConnection())
                {
                    conn.Open();
                    var query = "INSERT INTO address " +
                                "VALUES(0, @streetAddress, @city, @state, @zipcode)";
                    using (var cmd = new MySqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@streetAddress", patient.Address.Streetaddress);
                        cmd.Parameters.AddWithValue("@city", patient.Address.City);
                        cmd.Parameters.AddWithValue("@state", patient.Address.State);
                        cmd.Parameters.AddWithValue("@zipcode", patient.Address.Zipcode);

                        cmd.ExecuteNonQuery();
                    }

                    conn.Close();
                }
            }
            catch
            {
            }

            #endregion

            #region GetAddressID

            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT addressId " +
                            "FROM address " +
                            "WHERE streetAddress = @streetAddress " +
                            "AND city = @city " +
                            "AND state = @state";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@streetAddress", patient.Address.Streetaddress);
                    cmd.Parameters.AddWithValue("@city", patient.Address.City);
                    cmd.Parameters.AddWithValue("@state", patient.Address.State);
                    cmd.Parameters.AddWithValue("@zipcode", patient.Address.Zipcode);


                    using (var reader = cmd.ExecuteReader())
                    {
                        var addressOrdinal = reader.GetOrdinal("addressId");
                        while (reader.Read())
                            addressId = reader[addressOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[addressOrdinal];

                        conn.Close();
                    }
                }
            }

            #endregion

            #region insertPerson

            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "INSERT INTO person " +
                            "VALUES(@ssn, @pFName, @pLName, @sex, @birthdate, @addressId, @phoneNumber)";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@ssn", patient.Ssn);
                    cmd.Parameters.AddWithValue("@pFName", patient.FirstName);
                    cmd.Parameters.AddWithValue("@pLName", patient.LastName);
                    cmd.Parameters.AddWithValue("@sex", patient.Sex);
                    cmd.Parameters.AddWithValue("@birthdate", patient.Birthdate);
                    cmd.Parameters.AddWithValue("@addressId", addressId);
                    cmd.Parameters.AddWithValue("@phoneNumber", patient.PhoneNumber);

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }

            #endregion

            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "INSERT INTO patient " +
                            "VALUES(@patientId, @pssn, @primaryDoctorId)";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@patientId", patient.PatientId);
                    cmd.Parameters.AddWithValue("@pssn", patient.Ssn);
                    cmd.Parameters.AddWithValue("@primaryDoctorId", patient.PrimaryDoctor);

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }
        }

        public void EditPatient(Patient patient)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "UPDATE address SET streetAddress = @streetAddress, city = @city, state = @state, zipcode = @zipcode WHERE(address.addressID = @address)";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@streetAddress", patient.Address.Streetaddress);
                    cmd.Parameters.AddWithValue("@city", patient.Address.City);
                    cmd.Parameters.AddWithValue("@state", patient.Address.State);
                    cmd.Parameters.AddWithValue("@zipcode", patient.Address.Zipcode);
                    cmd.Parameters.AddWithValue("@address", patient.Address.AddressId);
                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }

            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "UPDATE person SET fname = @pFName, lname = @pLName, sex = @sex, addressId = @addressId, birthdate = @birthdate, phone = @phoneNumber WHERE(person.ssn = @ssn)";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@ssn", patient.Ssn);
                    cmd.Parameters.AddWithValue("@pFName", patient.FirstName);
                    cmd.Parameters.AddWithValue("@pLName", patient.LastName);
                    cmd.Parameters.AddWithValue("@sex", patient.Sex);
                    cmd.Parameters.AddWithValue("@birthdate", patient.Birthdate);
                    cmd.Parameters.AddWithValue("@addressId", patient.Address.AddressId);
                    cmd.Parameters.AddWithValue("@phoneNumber", patient.PhoneNumber);

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }


            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "UPDATE patient SET  primaryDoctor = @primaryDoctorId, pssn = @pssn WHERE (patient.patientID = @patientId)";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@patientId", patient.PatientId);
                    cmd.Parameters.AddWithValue("@pssn", patient.Ssn);
                    cmd.Parameters.AddWithValue("@primaryDoctorId", patient.PrimaryDoctor);

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }
        }

        public List<Patient> QueryPatientsByName(string searchText)
        {
            var patients = new List<Patient>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "SELECT person.*, address.*, patient.* FROM patient INNER JOIN person ON patient.pssn = person.ssn INNER JOIN address ON person.addressId = address.addressID WHERE person.lname LIKE @searchText";


                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@searchText", "%" + searchText + "%"));

                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("fname");
                        var pLNameOrdinal = reader.GetOrdinal("lname");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phone");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];


                            var address = new Address(aStreetAddress, aCity, aState, aZipcode);

                            patients.Add(new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor));
                        }
                    }
                }
            }

            return patients;
        }


        public List<Patient> QueryPatientsByLastName(string lastName)
        {
            var patients = new List<Patient>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "SELECT person.*, address.*, patient.* FROM patient INNER JOIN person ON patient.pssn = person.ssn INNER JOIN address ON person.addressId = address.addressID WHERE person.lname = @lastName;";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@lastName", MySqlDbType.VarChar));
                    cmd.Parameters["@lastName"].Value = lastName;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("fname");
                        var pLNameOrdinal = reader.GetOrdinal("lname");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phone");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];


                            var address = new Address(aStreetAddress, aCity, aState, aZipcode);

                            patients.Add(new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor));
                        }
                    }
                }
            }

            return patients;
        }


        public List<Patient> QueryPatientsByBirthDate(DateTime date)
        {
            var patients = new List<Patient>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "SELECT person.*, address.*, patient.* FROM patient INNER JOIN person ON patient.pssn = person.ssn INNER JOIN address ON person.addressId = address.addressID WHERE person.birthdate = @birthDate;";


                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@birthDate", MySqlDbType.Date));
                    cmd.Parameters["@birthDate"].Value = date.Date;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("fname");
                        var pLNameOrdinal = reader.GetOrdinal("lname");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phone");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];


                            var address = new Address(aStreetAddress, aCity, aState, aZipcode);

                            patients.Add(new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor));
                        }
                    }
                }
            }

            return patients;
        }

        public List<Patient> QueryPatientsBySsn(string ssn)
        {
            var patients = new List<Patient>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "SELECT person.*, address.*, patient.* FROM patient INNER JOIN person ON patient.pssn = person.ssn INNER JOIN address ON person.addressId = address.addressID WHERE person.ssn = @ssn;";


                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@ssn", MySqlDbType.VarChar));
                    cmd.Parameters["@ssn"].Value = ssn;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("fname");
                        var pLNameOrdinal = reader.GetOrdinal("lname");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phone");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];


                            var address = new Address(aStreetAddress, aCity, aState, aZipcode);

                            patients.Add(new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor));
                        }
                    }
                }
            }

            return patients;
        }

        public List<Patient> QueryPatientsByNameAndDate(string lastName, DateTime date)
        {
            var patients = new List<Patient>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "SELECT person.*, address.*, patient.* FROM patient INNER JOIN person ON patient.pssn = person.ssn INNER JOIN address ON person.addressId = address.addressID WHERE person.lname = @lastName AND person.birthdate = @birthDate;";


                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@lastName", MySqlDbType.VarChar));
                    cmd.Parameters["@lastName"].Value = lastName;
                    cmd.Parameters.Add(new MySqlParameter("@birthDate", MySqlDbType.Date));
                    cmd.Parameters["@birthDate"].Value = date.Date;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("fname");
                        var pLNameOrdinal = reader.GetOrdinal("lname");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phone");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];


                            var address = new Address(aStreetAddress, aCity, aState, aZipcode);

                            patients.Add(new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor));
                        }
                    }
                }
            }

            return patients;
        }

        public List<Patient> QueryPatientsByNameAndSsn(string lastName, string ssn)
        {
            var patients = new List<Patient>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "SELECT person.*, address.*, patient.* FROM patient INNER JOIN person ON patient.pssn = person.ssn INNER JOIN address ON person.addressId = address.addressID WHERE person.lname = @lastName AND person.ssn = @ssn;";


                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@lastName", MySqlDbType.VarChar));
                    cmd.Parameters["@lastName"].Value = lastName;
                    cmd.Parameters.Add(new MySqlParameter("@ssn", MySqlDbType.VarChar));
                    cmd.Parameters["@ssn"].Value = ssn;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("fname");
                        var pLNameOrdinal = reader.GetOrdinal("lname");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phone");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];


                            var address = new Address(aStreetAddress, aCity, aState, aZipcode);

                            patients.Add(new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor));
                        }
                    }
                }
            }

            return patients;
        }

        public List<Patient> QueryPatientsByDateAndSsn(DateTime date, string ssn)
        {
            var patients = new List<Patient>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "SELECT person.*, address.*, patient.* FROM patient INNER JOIN person ON patient.pssn = person.ssn INNER JOIN address ON person.addressId = address.addressID WHERE birthdate = @birthdate AND person.ssn = @ssn;";


                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@birthDate", MySqlDbType.Date));
                    cmd.Parameters["@birthDate"].Value = date.Date;
                    cmd.Parameters.Add(new MySqlParameter("@ssn", MySqlDbType.VarChar));
                    cmd.Parameters["@ssn"].Value = ssn;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("fname");
                        var pLNameOrdinal = reader.GetOrdinal("lname");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phone");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];


                            var address = new Address(aStreetAddress, aCity, aState, aZipcode);

                            patients.Add(new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor));
                        }
                    }
                }
            }

            return patients;
        }

        public List<Patient> QueryPatientsByDateSsnAndName(DateTime date, string ssn, string lastName)
        {
            var patients = new List<Patient>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "SELECT person.*, address.*, patient.* FROM patient INNER JOIN person ON patient.pssn = person.ssn INNER JOIN address ON person.addressId = address.addressID WHERE birthdate = @birthdate AND person.ssn = @ssn AND person.lname = @lastName;";


                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@birthDate", MySqlDbType.Date));
                    cmd.Parameters["@birthDate"].Value = date.Date;
                    cmd.Parameters.Add(new MySqlParameter("@ssn", MySqlDbType.VarChar));
                    cmd.Parameters["@ssn"].Value = ssn;
                    cmd.Parameters.Add(new MySqlParameter("@lastName", MySqlDbType.VarChar));
                    cmd.Parameters["@lastName"].Value = lastName;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientOrdinal = reader.GetOrdinal("patientID");
                        var pFNameOrdinal = reader.GetOrdinal("fname");
                        var pLNameOrdinal = reader.GetOrdinal("lname");
                        var pSexOrdinal = reader.GetOrdinal("sex");
                        var pBirthdateOrdinal = reader.GetOrdinal("birthdate");
                        var pPrimaryDoctorOrdinal = reader.GetOrdinal("primaryDoctor");
                        var pssnOrdinal = reader.GetOrdinal("pssn");
                        var pPhoneOrdinal = reader.GetOrdinal("phone");
                        var astreetAddressOrdinal = reader.GetOrdinal("streetAddress");
                        var acityOrdinal = reader.GetOrdinal("city");
                        var astateOrdinal = reader.GetOrdinal("state");
                        var azipcodeOrdinal = reader.GetOrdinal("zipcode");
                        while (reader.Read())
                        {
                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var pFName = reader[pFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pFNameOrdinal);

                            var pLName = reader[pLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pLNameOrdinal);

                            var pSex = reader[pSexOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(pSexOrdinal);

                            var pBirthdate = reader[pBirthdateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[pBirthdateOrdinal];

                            var pPrimaryDoctor = reader[pPrimaryDoctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pPrimaryDoctorOrdinal];

                            var pSsn = reader[pssnOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pssnOrdinal];

                            var pPhone = reader[pPhoneOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[pPhoneOrdinal];

                            var aStreetAddress = reader[astreetAddressOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astreetAddressOrdinal];

                            var aCity = reader[acityOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[acityOrdinal];

                            var aState = reader[astateOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[astateOrdinal];

                            var aZipcode = reader[azipcodeOrdinal] == DBNull.Value
                                ? default(string)
                                : (string) reader[azipcodeOrdinal];


                            var address = new Address(aStreetAddress, aCity, aState, aZipcode);

                            patients.Add(new Patient(patientId, pSsn, pFName, pLName, pSex, pBirthdate, address,
                                pPhone, pPrimaryDoctor));
                        }
                    }
                }
            }

            return patients;
        }
    }
}