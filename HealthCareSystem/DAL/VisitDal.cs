﻿using System;
using System.Collections.Generic;
using HealthCareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    public class VisitDal
    {
        /// <summary>
        ///     Gets all unfinalized visits.
        /// </summary>
        /// <returns></returns>
        public List<Visit> GetAllUnfinalizedVisits()
        {
            var visits = new List<Visit>();

            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT * " +
                            "FROM visit " +
                            "WHERE diagnosis is NULL";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var visitIdOrdinal = reader.GetOrdinal("visitID");
                        var appointmentOrdinal = reader.GetOrdinal("appID");
                        var prognosisOrdinal = reader.GetOrdinal("prognosis");
                        var notesOrdinal = reader.GetOrdinal("notes");

                        while (reader.Read())
                        {
                            var visitId = reader[visitIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[visitIdOrdinal];

                            var appointmentId = reader[appointmentOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[appointmentOrdinal];

                            var prognosis = reader[prognosisOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(prognosisOrdinal);

                            var notes = reader[notesOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(notesOrdinal);

                            var appointment = new AppointmentDal().GetAppointmentById(appointmentId);

                            var visit = new Visit(appointment, prognosis, notes);
                            visit.Id = visitId;

                            visits.Add(visit);
                        }
                    }
                }
            }

            return visits;
        }

        /// <summary>
        ///     Gets all finalized visits.
        /// </summary>
        /// <returns></returns>
        public List<Visit> GetAllFinalizedVisits()
        {
            var visits = new List<Visit>();

            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT * " +
                            "FROM visit " +
                            "WHERE diagnosis is not NULL";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var visitIdOrdinal = reader.GetOrdinal("visitID");
                        var appointmentOrdinal = reader.GetOrdinal("appID");
                        var prognosisOrdinal = reader.GetOrdinal("prognosis");
                        var notesOrdinal = reader.GetOrdinal("notes");
                        var diagnosisOrdinal = reader.GetOrdinal("diagnosis");

                        while (reader.Read())
                        {
                            var visitId = reader[visitIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[visitIdOrdinal];

                            var appointmentId = reader[appointmentOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[appointmentOrdinal];

                            var prognosis = reader[prognosisOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(prognosisOrdinal);

                            var notes = reader[notesOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(notesOrdinal);

                            var diagnosis = reader[diagnosisOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(diagnosisOrdinal);

                            var appointment = new AppointmentDal().GetAppointmentById(appointmentId);

                            var visit = new Visit(appointment, prognosis, notes);
                            visit.Id = visitId;
                            visit.Diagnosis = diagnosis;

                            visits.Add(visit);
                        }
                    }
                }
            }

            return visits;
        }

        /// <summary>
        ///     Adds the visit.
        /// </summary>
        /// <param name="visit">The visit.</param>
        public void AddVisit(Visit visit)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "INSERT INTO visit(appID, prognosis, notes) " +
                            "VALUES(@appID, @prognosis, @notes)";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@appID", visit.Appointment.AppointmentId);
                    cmd.Parameters.AddWithValue("@prognosis", visit.Prognosis);
                    cmd.Parameters.AddWithValue("@notes", visit.Notes);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        ///     Gets the visit by appointment.
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        /// <returns></returns>
        public Visit GetVisitByAppointment(Appointment appointment)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT visitID, prognosis, notes " +
                            "FROM visit " +
                            "WHERE appID = @apptID";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@apptID", appointment.AppointmentId);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var visitIdOrdinal = reader.GetOrdinal("visitID");
                        var prognosisOrdinal = reader.GetOrdinal("prognosis");
                        var notesOrdinal = reader.GetOrdinal("notes");

                        while (reader.Read())
                        {
                            var visitId = reader[visitIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[visitIdOrdinal];

                            var prognosis = reader[prognosisOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(prognosisOrdinal);

                            var notes = reader[notesOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(notesOrdinal);

                            var visit = new Visit(appointment, prognosis, notes);
                            visit.Id = visitId;

                            return visit;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        ///     Updates the visit.
        /// </summary>
        /// <param name="visit">The visit.</param>
        public void UpdateVisitDiagnosis(Visit visit)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "UPDATE visit " +
                            "SET diagnosis = @diagnosis " +
                            "WHERE visitId = @visitID";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@visitID", visit.Id);
                    cmd.Parameters.AddWithValue("@diagnosis", visit.Diagnosis);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        ///     Updates the visit.
        /// </summary>
        /// <param name="visit">The visit.</param>
        public void UpdateVisit(Visit visit)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "UPDATE visit " +
                            "SET prognosis = @prognosis, notes = @notes " +
                            "WHERE visitId = @visitID";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@visitID", visit.Id);
                    cmd.Parameters.AddWithValue("@prognosis", visit.Prognosis);
                    cmd.Parameters.AddWithValue("@notes", visit.Notes);

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}