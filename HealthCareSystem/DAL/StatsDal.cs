﻿using System;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    public class StatsDal
    {
        /// <summary>
        ///     Gets the patient count.
        /// </summary>
        /// <returns></returns>
        public int GetPatientCount()
        {
            var count = 0;
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT COUNT(*) " +
                            "FROM patient";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var patientCountOrdinal = reader.GetOrdinal("Count(*)");

                        while (reader.Read())
                        {
                            var patientCount = reader[patientCountOrdinal] == DBNull.Value
                                ? default(long)
                                : (long) reader[patientCountOrdinal];

                            count = Convert.ToInt32(patientCount);
                        }
                    }
                }
            }

            return count;
        }

        /// <summary>
        ///     Gets the appointment count.
        /// </summary>
        /// <returns></returns>
        public int GetAppointmentCount()
        {
            var count = 0;
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT COUNT(*) " +
                            "FROM appointment";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var apptCountOrdinal = reader.GetOrdinal("Count(*)");

                        while (reader.Read())
                        {
                            var apptCount = reader[apptCountOrdinal] == DBNull.Value
                                ? default(long)
                                : (long) reader[apptCountOrdinal];

                            count = Convert.ToInt32(apptCount);
                        }
                    }
                }
            }

            return count;
        }

        /// <summary>
        ///     Gets the visit count.
        /// </summary>
        /// <returns></returns>
        public int GetVisitCount()
        {
            var count = 0;
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT COUNT(*) " +
                            "FROM visit";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var visitCountOrdinal = reader.GetOrdinal("Count(*)");

                        while (reader.Read())
                        {
                            var visitCount = reader[visitCountOrdinal] == DBNull.Value
                                ? default(long)
                                : (long) reader[visitCountOrdinal];

                            count = Convert.ToInt32(visitCount);
                        }
                    }
                }
            }

            return count;
        }

        /// <summary>
        ///     Gets the appointment count in time frame.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <returns></returns>
        public int GetAppointmentCountInTimeFrame(DateTime start, DateTime end)
        {
            var count = 0;
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT COUNT(*) " +
                            "FROM appointment " +
                            "WHERE apptDate " +
                            "BETWEEN @start AND @end";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@start", start);
                    cmd.Parameters.AddWithValue("@end", end);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var apptCountOrdinal = reader.GetOrdinal("Count(*)");

                        while (reader.Read())
                        {
                            var apptCount = reader[apptCountOrdinal] == DBNull.Value
                                ? default(long)
                                : (long) reader[apptCountOrdinal];

                            count = Convert.ToInt32(apptCount);
                        }
                    }
                }
            }

            return count;
        }

        /// <summary>
        ///     Gets the visit count in time frame.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <returns></returns>
        public int GetVisitCountInTimeFrame(DateTime start, DateTime end)
        {
            var count = 0;
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT COUNT(*) " +
                            "FROM appointment, visit " +
                            "WHERE apptDate " +
                            "BETWEEN @start AND @end " +
                            "AND visit.appID = appointment.apptID";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@start", start);
                    cmd.Parameters.AddWithValue("@end", end);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var visitCountOrdinal = reader.GetOrdinal("Count(*)");

                        while (reader.Read())
                        {
                            var visitCount = reader[visitCountOrdinal] == DBNull.Value
                                ? default(long)
                                : (long) reader[visitCountOrdinal];

                            count = Convert.ToInt32(visitCount);
                        }
                    }
                }
            }

            return count;
        }
    }
}