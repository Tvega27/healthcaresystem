﻿using System;
using System.Collections.Generic;
using HealthCareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    /// <summary>
    /// dal for doctor related stuff.
    /// </summary>
    public class DoctorDal
    {
        /// <summary>
        ///     Gets all doctors.
        /// </summary>
        /// <returns></returns>
        public List<Doctor> GetAllDoctors()
        {
            var query = "SELECT doctorID, doctor_uID, fieldName, userID, userSSN, ssn, fname, lname " +
                        "FROM doctors, `user`, `field`, person " +
                        "WHERE doctor_uID = userID " +
                        "AND doctors.fieldID = `field`.fieldID " +
                        "AND `user`.userSSN = person.ssn";
            var doctors = new List<Doctor>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                using (var cmd = new MySqlCommand(query, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var doctorOrdinal = reader.GetOrdinal("doctorID");
                        var doctorUidOrdinal = reader.GetOrdinal("doctor_uID");
                        var dSsnOrdinal = reader.GetOrdinal("userSSN");
                        var dFNameOrdinal = reader.GetOrdinal("fname");
                        var dLNameOrdinal = reader.GetOrdinal("lname");
                        var dFieldOrdinal = reader.GetOrdinal("fieldName");

                        while (reader.Read())
                        {
                            var doctorId = reader[doctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[doctorOrdinal];

                            var doctorUid = reader[doctorUidOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[doctorUidOrdinal];

                            var dSsn = reader[dSsnOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(dSsnOrdinal);

                            var dFName = reader[dFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(dFNameOrdinal);

                            var dLName = reader[dLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(dLNameOrdinal);

                            var dField = reader[dFieldOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(dFieldOrdinal);

                            doctors.Add(new Doctor(doctorId, doctorUid, dSsn, dFName, dLName, dField));
                        }
                    }
                }
            }

            return doctors;
        }

        /// <summary>
        /// Gets the doctor by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Doctor GetDoctorById(int id)
        {
            var query = "SELECT doctorID, doctor_uID, fieldName, userID, userSSN, ssn, fname, lname " +
                        "FROM doctors, `user`, `field`, person " +
                        "WHERE doctor_uID = userID " +
                        "AND doctors.fieldID = `field`.fieldID " +
                        "AND `user`.userSSN = person.ssn " +
                        "AND doctorID = @id";

            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var doctorOrdinal = reader.GetOrdinal("doctorID");
                        var doctorUidOrdinal = reader.GetOrdinal("doctor_uID");
                        var dSsnOrdinal = reader.GetOrdinal("userSSN");
                        var dFNameOrdinal = reader.GetOrdinal("fname");
                        var dLNameOrdinal = reader.GetOrdinal("lname");
                        var dFieldOrdinal = reader.GetOrdinal("fieldName");

                        while (reader.Read())
                        {
                            var doctorId = reader[doctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[doctorOrdinal];

                            var doctorUid = reader[doctorUidOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[doctorUidOrdinal];

                            var dSsn = reader[dSsnOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(dSsnOrdinal);

                            var dFName = reader[dFNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(dFNameOrdinal);

                            var dLName = reader[dLNameOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(dLNameOrdinal);

                            var dField = reader[dFieldOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(dFieldOrdinal);

                            return new Doctor(doctorId, doctorUid, dSsn, dFName, dLName, dField);
                        }
                    }
                }
            }

            return null;
        }


        /// <summary>
        ///     Queries the name of the doctors by.
        /// </summary>
        /// <param name="userInput">The user input.</param>
        /// <returns></returns>
        public List<Doctor> QueryDoctorsByName(string searchText)
        {
            var doctors = new List<Doctor>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT doctorID, doctor_uID, fieldName, userID, userSSN, ssn, fname, lname " +
                            "FROM doctors, `user`, `field`, person " +
                            "WHERE doctor_uID = userID " +
                            "AND doctors.fieldID = `field`.fieldID " +
                            "AND `user`.userSSN = person.ssn " +
                            "AND lname LIKE @searchText";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("@searchText", "%" + searchText + "%"));

                    using (var reader = cmd.ExecuteReader())
                    {
                        var doctorIdOrdinal = reader.GetOrdinal("doctorID");
                        var doctorUidOrdinal = reader.GetOrdinal("doctor_uID");
                        var dSsnOrdinal = reader.GetOrdinal("userSSN");
                        var fnameOrdinal = reader.GetOrdinal("fname");
                        var lnameOrdinal = reader.GetOrdinal("lname");
                        var dFieldOrdinal = reader.GetOrdinal("fieldName");

                        while (reader.Read())
                        {
                            var doctorId = reader[doctorIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[doctorIdOrdinal];

                            var doctorUid = reader[doctorUidOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[doctorUidOrdinal];

                            var dSsn = reader[dSsnOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(dSsnOrdinal);

                            var fname = reader[fnameOrdinal] == DBNull.Value ? null : reader.GetString(fnameOrdinal);

                            var lname = reader[lnameOrdinal] == DBNull.Value ? null : reader.GetString(lnameOrdinal);

                            var dField = reader[dFieldOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(dFieldOrdinal);

                            doctors.Add(new Doctor(doctorId, doctorUid, dSsn, fname, lname, dField));
                        }
                    }
                }
            }

            return doctors;
        }
    }
}