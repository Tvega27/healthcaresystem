﻿using System;
using HealthCareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    /// <summary>
    /// DAL for routine checks
    /// </summary>
    public class RoutineCheckDal
    {
        /// <summary>
        /// Adds the routine check.
        /// </summary>
        /// <param name="check">The check.</param>
        public void AddRoutineCheck(RoutineCheck check)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query =
                    "INSERT INTO routineCheck(visitID, bpSystolic, bpDiastolic, oxygenSaturation, pulseBPM, height, weight, bodyTemp) " +
                    "VALUES(@visitID, @bpSystolic, @bpDiastolic, @oxygenSaturation, @pulseBPM, @height, @weight, @bodyTemp)";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@visitID", check.VisitId);
                    cmd.Parameters.AddWithValue("@bpSystolic", check.BpSystolic);
                    cmd.Parameters.AddWithValue("@bpDiastolic", check.BpDiastolic);
                    cmd.Parameters.AddWithValue("@oxygenSaturation", check.OxygenSaturation);
                    cmd.Parameters.AddWithValue("@pulseBPM", check.PulseBpm);
                    cmd.Parameters.AddWithValue("@height", check.Height);
                    cmd.Parameters.AddWithValue("@weight", check.Weight);
                    cmd.Parameters.AddWithValue("@bodyTemp", check.BodyTemp);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Updates the routine check.
        /// </summary>
        /// <param name="check">The check.</param>
        public void UpdateRoutineCheck(RoutineCheck check)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "UPDATE routineCheck " +
                            "SET bpSystolic = @bpSystolic, bpDiastolic = @bpDiastolic, oxygenSaturation = @oxygenSaturation, " +
                            "pulseBPM = @pulseBPM, height = @height, weight = @weight, bodyTemp = @bodyTemp " +
                            "WHERE visitID = @visitID";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@visitID", check.VisitId);
                    cmd.Parameters.AddWithValue("@bpSystolic", check.BpSystolic);
                    cmd.Parameters.AddWithValue("@bpDiastolic", check.BpDiastolic);
                    cmd.Parameters.AddWithValue("@oxygenSaturation", check.OxygenSaturation);
                    cmd.Parameters.AddWithValue("@pulseBPM", check.PulseBpm);
                    cmd.Parameters.AddWithValue("@height", check.Height);
                    cmd.Parameters.AddWithValue("@weight", check.Weight);
                    cmd.Parameters.AddWithValue("@bodyTemp", check.BodyTemp);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        ///     Gets the routine check by visit.
        /// </summary>
        /// <param name="visit">The visit identifier.</param>
        /// <returns></returns>
        public RoutineCheck GetRoutineCheckByVisit(int visit)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT *" +
                            "FROM routineCheck " +
                            "WHERE visitID = @visitID";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@visitID", visit);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var visitIdOrdinal = reader.GetOrdinal("visitID");
                        var bpSystolicOrdinal = reader.GetOrdinal("bpSystolic");
                        var bpDiastolicOrdinal = reader.GetOrdinal("bpDiastolic");
                        var oxygenOrdinal = reader.GetOrdinal("oxygenSaturation");
                        var pulseBpmOrdinal = reader.GetOrdinal("pulseBPM");
                        var heightOrdinal = reader.GetOrdinal("height");
                        var weightOrdinal = reader.GetOrdinal("weight");
                        var bodyTempOrdinal = reader.GetOrdinal("bodyTemp");

                        while (reader.Read())
                        {
                            var visitId = reader[visitIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[visitIdOrdinal];

                            var bpSystolic = reader[bpSystolicOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[bpSystolicOrdinal];

                            var bpDiastolic = reader[bpDiastolicOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[bpDiastolicOrdinal];

                            var oxygenStaturation = reader[oxygenOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[oxygenOrdinal];

                            var pulseBpm = reader[pulseBpmOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[pulseBpmOrdinal];

                            var height = reader[heightOrdinal] == DBNull.Value
                                ? default(decimal)
                                : (decimal) reader[heightOrdinal];

                            var weight = reader[weightOrdinal] == DBNull.Value
                                ? default(decimal)
                                : (decimal) reader[weightOrdinal];

                            var bodyTemp = reader[bodyTempOrdinal] == DBNull.Value
                                ? default(decimal)
                                : (decimal) reader[bodyTempOrdinal];

                            return new RoutineCheck(visitId, bpSystolic, bpDiastolic, oxygenStaturation, pulseBpm,
                                height, weight, bodyTemp);
                        }
                    }
                }
            }

            return null;
        }
    }
}