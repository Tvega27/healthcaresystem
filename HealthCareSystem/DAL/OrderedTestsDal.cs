﻿using System;
using System.Collections.Generic;
using HealthCareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    /// <summary>
    /// dal for ordered tests
    /// </summary>
    public class OrderedTestsDal
    {
        /// <summary>
        ///     Gets the ordered tests by visit.
        /// </summary>
        /// <param name="visitId">The visit identifier.</param>
        /// <returns></returns>
        public List<OrderedTest> GetOrderedTestsByVisit(int visit)
        {
            var tests = new List<OrderedTest>();

            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var selectQuery = "SELECT * " +
                                  "FROM ordered_tests " +
                                  "WHERE visitID = @visitID ";

                using (var cmd = new MySqlCommand(selectQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@visitID", visit);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var testCodeOrdinal = reader.GetOrdinal("code");
                        var visitIdOrdinal = reader.GetOrdinal("visitID");
                        var resultOrdinal = reader.GetOrdinal("result");
                        var orderDateOrdinal = reader.GetOrdinal("orderDate");
                        var completedDateOrdinal = reader.GetOrdinal("completedDate");

                        while (reader.Read())
                        {
                            var code = reader[testCodeOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(testCodeOrdinal);

                            var visitId = reader[visitIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[visitIdOrdinal];

                            var result = reader[resultOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(resultOrdinal);

                            var orderDate = reader[orderDateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[orderDateOrdinal];

                            var completedDate = reader[completedDateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[completedDateOrdinal];

                            var test = new TestDal().GetTest(code);

                            var orderedTest = new OrderedTest(test, visit, orderDate);
                            orderedTest.Results = result;
                            orderedTest.CompletedDate = completedDate;

                            tests.Add(orderedTest);
                        }
                    }
                }
            }

            return tests;
        }

        /// <summary>
        ///     Adds the ordered test.
        /// </summary>
        /// <param name="test">The test.</param>
        public void AddOrderedTest(OrderedTest test)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "INSERT INTO ordered_tests " +
                            "VALUES(@code, @visitID, @result, @orderDate, null)";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@code", test.Test.Code);
                    cmd.Parameters.AddWithValue("@visitID", test.VisitId);
                    cmd.Parameters.AddWithValue("@result", test.Results);
                    cmd.Parameters.AddWithValue("@orderDate", test.OrderDate);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        ///     Updates the ordered test.
        /// </summary>
        /// <param name="test">The test.</param>
        public void UpdateOrderedTest(OrderedTest test)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "UPDATE ordered_tests " +
                            "SET result = @result, completedDate = @completedDate " +
                            "WHERE code = @code " +
                            "AND visitID = @visitID";

                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@code", test.Test.Code);
                    cmd.Parameters.AddWithValue("@visitID", test.VisitId);
                    cmd.Parameters.AddWithValue("@result", test.Results);
                    cmd.Parameters.AddWithValue("@completedDate", test.CompletedDate);

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}