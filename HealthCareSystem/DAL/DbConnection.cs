﻿using System;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    /// <summary>
    /// DB connection
    /// </summary>
    public class DbConnection
    {
        #region Methods

        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <returns></returns>
        public static MySqlConnection GetConnection()
        {
            var conStr = "server=160.10.25.16; port=3306; uid=cs3230f18h;" +
                         "pwd=0My09txTjqHofOmu; database=cs3230f18h;";
            MySqlConnection conn = null;
            try
            {
                using (conn = new MySqlConnection(conStr))
                {
                    conn.Open();
                }
            }

            catch (MySqlException mex)
            {
                Console.WriteLine(mex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return conn;
        }

        #endregion
    }
}