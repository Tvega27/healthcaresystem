﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    /// <summary>Data access layer for admin functionality</summary>
    public class AdminQueryDal
    {
        /// <summary> runs a select statement</summary>
        /// <param name="command"></param>
        public DataTable ExecuteSelectStatement(string command)
        {
            try
            {
                var dataTable = new DataTable();
                using (var connection = DbConnection.GetConnection())
                {
                    connection.Open();
                    var sqlcommand = connection.CreateCommand();
                    sqlcommand.CommandText = command;
                    using (var dataAdapter = new MySqlDataAdapter(sqlcommand))
                    {
                        dataAdapter.Fill(dataTable);
                    }
                }

                return dataTable;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        /// <summary> runs a non select statement</summary>
        /// <param name="command"></param>
        public bool NonSelectCommand(string statement)
        {
            var executed = true;
            try
            {
                using (var conn = DbConnection.GetConnection())
                {
                    conn.Open();


                    using (var cmd = new MySqlCommand(statement, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                executed = false;
            }

            return executed;
        }
    }
}