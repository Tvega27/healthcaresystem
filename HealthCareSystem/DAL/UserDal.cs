﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using HealthCareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    public static class UserDal
    {
        public static bool CheckCredentials(string user, string pass)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var selectQuery = "SELECT count(*) FROM `user` WHERE(username = @username) and (password = @password);";


                using (var cmd = new MySqlCommand(selectQuery, conn))
                {
                    cmd.Parameters.Add("@username", MySqlDbType.VarChar);
                    cmd.Parameters["@username"].Value = user;

                    cmd.Parameters.Add("@password", MySqlDbType.VarChar);
                    var sha256Pass = sha256_hash(pass);
                    cmd.Parameters["@password"].Value = sha256Pass;
                    var count = Convert.ToInt32(cmd.ExecuteScalar());
                    if (count == 1)
                        return true;
                    return false;
                }
            }
        }

        public static bool CheckAdmin(string user)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var selectQuery =
                    "SELECT  `user`.username FROM `user` INNER JOIN admin ON `user`.userID = admin.admin_uId WHERE(`user`.username = @username)";


                using (var cmd = new MySqlCommand(selectQuery, conn))
                {
                    cmd.Parameters.Add("@username", MySqlDbType.VarChar);
                    cmd.Parameters["@username"].Value = user;

                    try
                    {
                        var count = Convert.ToInt32(cmd.ExecuteScalar());
                        return true;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
            }
        }

        public static string GetNameFromId(int id)
        {
            var name = "";
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT fname, lname " +
                            "FROM user, person " +
                            "WHERE userID = @userID " +
                            "AND userSSN = ssn";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@userID", id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var fnameOrdinal = reader.GetOrdinal("fname");
                        var lnameOrdinal = reader.GetOrdinal("lname");


                        while (reader.Read())
                        {
                            var fname = reader[fnameOrdinal] == DBNull.Value ? null : reader.GetString(fnameOrdinal);

                            var lname = reader[lnameOrdinal] == DBNull.Value ? null : reader.GetString(lnameOrdinal);
                            name = fname + " " + lname;
                        }
                    }
                }
            }

            return name;
        }

        public static string GetNameFromUsername(string userName)
        {
            var name = "";
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT person.fname, person.lname " +
                            "FROM `user` WHERE (username = @username) INNER JOIN person ON `user`.userSSN = person.ssn";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@username", userName);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var fnameOrdinal = reader.GetOrdinal("person.fname");
                        var lnameOrdinal = reader.GetOrdinal("person.lname");


                        while (reader.Read())
                        {
                            var fname = reader[fnameOrdinal] == DBNull.Value ? null : reader.GetString(fnameOrdinal);

                            var lname = reader[lnameOrdinal] == DBNull.Value ? null : reader.GetString(lnameOrdinal);
                            name = fname + " " + lname;
                        }
                    }
                }
            }

            return name;
        }

        /// <summary>
        ///     Gets the nurse.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public static Nurse GetNurse(string username)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var selectQuery = "SELECT * " +
                                  "FROM nurse, user, person " +
                                  "WHERE nurse.nurse_uId = user.userID " +
                                  "AND user.userSSN = person.ssn " +
                                  "AND username = @username";


                using (var cmd = new MySqlCommand(selectQuery, conn))
                {
                    cmd.Parameters.Add("@username", MySqlDbType.VarChar);
                    cmd.Parameters["@username"].Value = username;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var fnameOrdinal = reader.GetOrdinal("fname");
                        var lnameOrdinal = reader.GetOrdinal("lname");
                        var nurseIdOrdinal = reader.GetOrdinal("nID");
                        var nurseUserIdOrdinal = reader.GetOrdinal("nurse_uId");


                        while (reader.Read())
                        {
                            var fname = reader[fnameOrdinal] == DBNull.Value ? null : reader.GetString(fnameOrdinal);

                            var lname = reader[lnameOrdinal] == DBNull.Value ? null : reader.GetString(lnameOrdinal);

                            var nurseId = reader[nurseIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[nurseIdOrdinal];

                            var nurseUId = reader[nurseUserIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[nurseUserIdOrdinal];

                            var nurse = new Nurse(nurseId, nurseId);
                            nurse.FirstName = fname;
                            nurse.LastName = lname;
                            return nurse;
                        }
                    }
                }
            }

            return null;
        }

        private static string sha256_hash(string value)
        {
            using (var hash = SHA256.Create())
            {
                return string.Concat(hash
                    .ComputeHash(Encoding.UTF8.GetBytes(value))
                    .Select(item => item.ToString("x2")));
            }
        }
    }
}