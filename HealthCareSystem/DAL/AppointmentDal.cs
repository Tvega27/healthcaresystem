﻿using System;
using System.Collections.Generic;
using HealthCareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthCareSystem.DAL
{
    /// <summary>
    ///     DAL for appointment functionallity
    /// </summary>
    public class AppointmentDal
    {
        /// <summary>
        ///     Gets the upcoming appointments.
        /// </summary>
        /// <returns></returns>
        public List<Appointment> GetUpcomingAppointments()
        {
            var appointments = new List<Appointment>();
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var currentDate = DateTime.Now;
                var currentTime = currentDate.TimeOfDay;
                var selectQuery = "SELECT * " +
                                  "FROM appointment ";

                using (var cmd = new MySqlCommand(selectQuery, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var apptIdOrdinal = reader.GetOrdinal("apptID");
                        var dateOrdinal = reader.GetOrdinal("apptDate");

                        var patientOrdinal = reader.GetOrdinal("patientID");

                        var doctorOrdinal = reader.GetOrdinal("doctorID");

                        var reasonOrdinal = reader.GetOrdinal("reasonForVisit");
                        var completedOrdinal = reader.GetOrdinal("completed");

                        while (reader.Read())
                        {
                            var apptId = reader[apptIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[apptIdOrdinal];

                            var date = reader[dateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[dateOrdinal];

                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var doctorId = reader[doctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[doctorOrdinal];

                            var reason = reader[reasonOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(reasonOrdinal);

                            var completed = reader[completedOrdinal] != DBNull.Value && (bool) reader[completedOrdinal];

                            var patient = new PatientDal().GetPatient(patientId);

                            var doctor = new DoctorDal().GetDoctorById(doctorId);

                            appointments.Add(new Appointment(apptId, patient, doctor, date, reason, completed));
                        }
                    }
                }
            }

            return appointments;
        }

        /// <summary>
        ///     Gets the appointment by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Appointment GetAppointmentById(int id)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var selectQuery = "SELECT * " +
                                  "FROM appointment " +
                                  "WHERE apptID = @id";

                using (var cmd = new MySqlCommand(selectQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var apptIdOrdinal = reader.GetOrdinal("apptID");
                        var dateOrdinal = reader.GetOrdinal("apptDate");

                        var patientOrdinal = reader.GetOrdinal("patientID");

                        var doctorOrdinal = reader.GetOrdinal("doctorID");

                        var reasonOrdinal = reader.GetOrdinal("reasonForVisit");
                        var completedOrdinal = reader.GetOrdinal("completed");

                        while (reader.Read())
                        {
                            var apptId = reader[apptIdOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[apptIdOrdinal];

                            var date = reader[dateOrdinal] == DBNull.Value
                                ? default(DateTime)
                                : (DateTime) reader[dateOrdinal];

                            var patientId = reader[patientOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[patientOrdinal];

                            var doctorId = reader[doctorOrdinal] == DBNull.Value
                                ? default(int)
                                : (int) reader[doctorOrdinal];

                            var reason = reader[reasonOrdinal] == DBNull.Value
                                ? null
                                : reader.GetString(reasonOrdinal);

                            var completed = reader[completedOrdinal] != DBNull.Value && (bool) reader[completedOrdinal];

                            var patient = new PatientDal().GetPatient(patientId);

                            var doctor = new DoctorDal().GetDoctorById(doctorId);

                            return new Appointment(apptId, patient, doctor, date, reason, completed);
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        ///     Adds the appointment.
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        public void AddAppointment(Appointment appointment)
        {
            using (var conn = DbConnection.GetConnection())
            {
                conn.Open();
                var query = "INSERT INTO appointment(apptDate, patientID, doctorID, reasonForVisit, completed) " +
                            "VALUES(@date, @patientId, @doctorId, @reasons, @completed)";
                using (var cmd = new MySqlCommand(query, conn))
                {
                    appointment.SetAppointmentTimeToDate();
                    cmd.Parameters.AddWithValue("@date", appointment.Date);
                    cmd.Parameters.AddWithValue("@patientId", appointment.Patient.PatientId);
                    cmd.Parameters.AddWithValue("@doctorId", appointment.Doctor.DoctorId);
                    cmd.Parameters.AddWithValue("@reasons", appointment.Reasons);
                    cmd.Parameters.AddWithValue("@completed", appointment.Completed);

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }
        }
    }
}