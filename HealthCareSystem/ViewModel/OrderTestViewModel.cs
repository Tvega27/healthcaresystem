﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using HealthCareSystem.DAL;
using HealthCareSystem.Model;

namespace HealthCareSystem.ViewModel
{
    public class OrderTestViewModel : INotifyPropertyChanged
    {
        private Appointment _appointment;
        private readonly OrderedTestsDal _orderTestDal;

        private readonly TestDal _testDal;

        private List<Test> _tests;

        private string _testText;

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrderTestViewModel" /> class.
        /// </summary>
        public OrderTestViewModel()
        {
            _testDal = new TestDal();
            _orderTestDal = new OrderedTestsDal();
        }

        /// <summary>
        ///     Gets or sets the test text.
        /// </summary>
        /// <value>
        ///     The test text.
        /// </value>
        public string TestText
        {
            get => _testText;
            set
            {
                _testText = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        ///     Gets or sets the tests.
        /// </summary>
        /// <value>
        ///     The tests.
        /// </value>
        public List<Test> Tests
        {
            get => _tests;
            set
            {
                _tests = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the visit.
        /// </summary>
        /// <value>
        ///     The visit.
        /// </value>
        public Appointment Appointment
        {
            get => _appointment;
            set
            {
                _appointment = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the visit.
        /// </summary>
        /// <value>
        ///     The visit.
        /// </value>
        public Visit Visit { get; set; }

        /// <summary>
        ///     Gets or sets the test to order.
        /// </summary>
        /// <value>
        ///     The test to order.
        /// </value>
        public Test TestToOrder { get; set; }

        /// <summary>
        ///     Occurs when a property value changes.
        /// </summary>
        /// <returns></returns>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Tests the box text changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="AutoSuggestBoxTextChangedEventArgs" /> instance containing the event data.</param>
        public void TestBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (sender.Text.Length > 1)
                Tests = Tests.Count != 0
                    ? _testDal.GetTestsByName(sender.Text)
                    : new List<Test> {new Test("No Matching Tests...", string.Empty)};
            else
                Tests = new List<Test> {new Test("No Matching Tests...", string.Empty)};
            sender.ItemsSource = Tests;
        }

        /// <summary>
        ///     Tests the box suggestion chosen.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="AutoSuggestBoxSuggestionChosenEventArgs" /> instance containing the event data.</param>
        public void TestBox_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            if (!args.SelectedItem.Equals("No Matching Tests..."))
            {
                var test = (Test) args.SelectedItem;
                sender.Text = test.CodeAndName;
                TestToOrder = test;
            }
            else
            {
                sender.Text = string.Empty;
            }
        }

        /// <summary>
        ///     Orders the test click.
        /// </summary>
        public async void OrderTestClick()
        {
            try
            {
                _orderTestDal.AddOrderedTest(new OrderedTest(TestToOrder, Visit.Id, DateTime.Now));
                var msgBox = new MessageDialog("Test Ordered");
                msgBox.Content = "\nThe test was successfully ordered.";
                msgBox.Title = "Test Ordered";
                await msgBox.ShowAsync();
            }
            catch (Exception arg)
            {
                var msgBox = new MessageDialog(arg.Message);
                msgBox.Content =
                    "\nMake sure the test is entered correctly, \nor the test order already exists for this visit.";
                msgBox.Title = "Something Went Wrong...";
                await msgBox.ShowAsync();
            }
        }

        /// <summary>
        ///     Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}