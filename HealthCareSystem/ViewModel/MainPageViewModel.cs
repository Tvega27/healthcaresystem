﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.ApplicationModel.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using HealthCareSystem.DAL;
using HealthCareSystem.Model;

namespace HealthCareSystem.ViewModel
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        private readonly AppointmentDal _appointmentDb;
        private readonly DoctorDal _doctorDb;
        private readonly PatientDal _patientDb;
        private List<Appointment> _appointments;
        private Appointment _appointmentToRegister;
        private DateTimeOffset _dateForAppt;
        private string _doctorBoxText;
        private List<Doctor> _doctorNames;

        private string _patientBoxText;
        private List<Patient> _patientNames;
        private string _reasonForAppt;
        private Appointment _selectedAppointment;
        private TimeSpan _timeForAppt;
        private bool _validAppointmentChanged;

        private bool _visitBtnEnable;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainPageViewModel" /> class.
        /// </summary>
        public MainPageViewModel()
        {
            _appointmentDb = new AppointmentDal();
            _patientDb = new PatientDal();
            _doctorDb = new DoctorDal();
            _doctorNames = new List<Doctor>();
            _patientNames = new List<Patient>();
            _dateForAppt = DateTime.Now;
            _timeForAppt = DateTime.Now.TimeOfDay;
            AppointmentToRegister = new Appointment(null, null, _dateForAppt.Date, "", false);
            _validAppointmentChanged = true;

            UpdateAppointments();
        }

        public Appointment AppointmentToRegister
        {
            get => _appointmentToRegister;
            set
            {
                _appointmentToRegister = value;
                OnPropertyChanged();
                if (_appointmentToRegister.Patient != null && _appointmentToRegister.Doctor != null)
                    _validAppointmentChanged = true;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether [valid appointment changed].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [valid appointment changed]; otherwise, <c>false</c>.
        /// </value>
        public bool ValidAppointmentChanged
        {
            get => _validAppointmentChanged;
            set
            {
                _validAppointmentChanged = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the appointments.
        /// </summary>
        /// <value>
        ///     The appointments.
        /// </value>
        public List<Appointment> Appointments
        {
            get => _appointments;
            set
            {
                _appointments = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the patient for appt.
        /// </summary>
        /// <value>
        ///     The patient for appt.
        /// </value>
        public string PatientBoxText
        {
            get => _patientBoxText;
            set
            {
                _patientBoxText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the doctor for appt.
        /// </summary>
        /// <value>
        ///     The doctor for appt.
        /// </value>
        public string DoctorBoxText
        {
            get => _doctorBoxText;
            set
            {
                _doctorBoxText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the date for appt.
        /// </summary>
        /// <value>
        ///     The date for appt.
        /// </value>
        public DateTimeOffset DateForAppt
        {
            get => _dateForAppt;
            set
            {
                _dateForAppt = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the time for appt.
        /// </summary>
        /// <value>
        ///     The time for appt.
        /// </value>
        public TimeSpan TimeForAppt
        {
            get => _timeForAppt;
            set
            {
                _timeForAppt = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the reason for appt.
        /// </summary>
        /// <value>
        ///     The reason for appt.
        /// </value>
        public string ReasonForAppt
        {
            get => _reasonForAppt;
            set
            {
                _reasonForAppt = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the selected appointment.
        /// </summary>
        /// <value>
        ///     The selected appointment.
        /// </value>
        public Appointment SelectedAppointment
        {
            get => _selectedAppointment;
            set
            {
                _selectedAppointment = value;
                if (_selectedAppointment != null)
                    VisitBtnEnable = true;
                else
                    VisitBtnEnable = false;
                OnPropertyChanged();
            }
        }

        public bool VisitBtnEnable
        {
            get => _visitBtnEnable;
            set
            {
                _visitBtnEnable = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Occurs when a property value changes.
        /// </summary>
        /// <returns></returns>
        public event PropertyChangedEventHandler PropertyChanged;

        private void UpdateAppointments()
        {
            Appointments = _appointmentDb.GetUpcomingAppointments();
        }

        /// <summary>
        ///     Doctors the box text changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="AutoSuggestBoxTextChangedEventArgs" /> instance containing the event data.</param>
        public void DoctorBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (sender.Text.Length > 1)
                _doctorNames = _doctorNames.Count != 0
                    ? _doctorDb.QueryDoctorsByName(sender.Text)
                    : new List<Doctor> {new Doctor(0, 0, "", "No Matching", "Doctors...", "")};
            else
                _doctorNames = new List<Doctor> {new Doctor(0, 0, "", "No Matching", "Doctors...", "")};
            sender.ItemsSource = _doctorNames;
        }

        /// <summary>
        ///     Doctors the box suggestion chosen.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="AutoSuggestBoxSuggestionChosenEventArgs" /> instance containing the event data.</param>
        public void DoctorBox_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            if (!args.SelectedItem.Equals("No Matching Doctors..."))
            {
                var doctor = (Doctor) args.SelectedItem;
                sender.Text = doctor.DoctorFullName;
                AppointmentToRegister.Doctor = doctor;
            }
            else
            {
                sender.Text = string.Empty;
            }
        }

        public void PatientBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (sender.Text.Length > 1)
                _patientNames = _patientNames.Count != 0
                    ? _patientDb.QueryPatientsByName(sender.Text)
                    : new List<Patient>
                    {
                        new Patient(0, "", "No Matching", "Patients...", "", new DateTime(), new Address(), "", 0)
                    };
            else
                _patientNames = new List<Patient>
                {
                    new Patient(0, "", "No Matching", "Patients...", "", new DateTime(), new Address(), "", 0)
                };
            sender.ItemsSource = _patientNames;
        }

        public void PatientBox_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            if (!args.SelectedItem.Equals("No Matching Patients..."))
            {
                var patient = (Patient) args.SelectedItem;
                sender.Text = patient.PatientFullName;
                AppointmentToRegister.Patient = patient;
            }
            else
            {
                sender.Text = string.Empty;
            }
        }

        public async void Logout()
        {
            var result = await CoreApplication.RequestRestartAsync("Application Restart Programmatically ");

            if (result == AppRestartFailureReason.NotInForeground ||
                result == AppRestartFailureReason.RestartPending ||
                result == AppRestartFailureReason.Other)
            {
                var msgBox = new MessageDialog("Logout failed", result.ToString());
                await msgBox.ShowAsync();
            }
        }

        /// <summary>
        ///     Registers the appointment upon button click.
        /// </summary>
        public async void RegisterAppointmentClick()
        {
            try
            {
                var date = DateForAppt.Date + TimeForAppt;
                AppointmentToRegister.Date = date;
                AppointmentToRegister.Reasons = ReasonForAppt;

                _appointmentDb.AddAppointment(AppointmentToRegister);
                UpdateAppointments();
                var msgBox = new MessageDialog("Appointment Added");
                await msgBox.ShowAsync();
            }
            catch (Exception arg)
            {
                var msgBox = new MessageDialog(arg.Message);
                msgBox.Content = "\nMake sure all the fields are entered correctly.";
                msgBox.Title = "Something Went Wrong...";
                await msgBox.ShowAsync();
            }

            UpdateAppointments();
        }

        public async void AddVisitButtonClick()
        {
            var visitDal = new VisitDal();
            try
            {
                visitDal.AddVisit(new Visit(SelectedAppointment, string.Empty, string.Empty));
                var msgBox = new MessageDialog("Visit Added");
                msgBox.Content = "\nThe visit was successfully added.";
                msgBox.Title = "Visit Added";
                await msgBox.ShowAsync();
            }
            catch (Exception ex)
            {
                var msgBox = new MessageDialog(ex.Message);
                msgBox.Content = "\nThis visit is already established. Changes will update the visit.";
                msgBox.Title = "Established Visit";
                await msgBox.ShowAsync();
            }
        }

        /// <summary>
        ///     Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}