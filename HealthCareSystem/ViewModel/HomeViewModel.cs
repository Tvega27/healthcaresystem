﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using HealthCareSystem.DAL;

namespace HealthCareSystem.ViewModel
{
    public class HomeViewModel : INotifyPropertyChanged
    {
        private string _appointmentCountText;

        private DateTimeOffset _endDate;

        private DateTimeOffset _startDate;
        private readonly StatsDal _statsDal;

        private string _totalAppointmentCountText;
        private string _totalPatientCountText;

        private string _totalVisitCountText;

        private string _visitCountText;

        /// <summary>
        ///     Initializes a new instance of the <see cref="HomeViewModel" /> class.
        /// </summary>
        public HomeViewModel()
        {
            _statsDal = new StatsDal();
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
            UpdateTotalStats();
        }

        /// <summary>
        ///     Gets or sets the total patient count text.
        /// </summary>
        /// <value>
        ///     The total patient count text.
        /// </value>
        public string TotalPatientCountText
        {
            get => _totalPatientCountText;
            set
            {
                _totalPatientCountText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the total appointment count text.
        /// </summary>
        /// <value>
        ///     The total appointment count text.
        /// </value>
        public string TotalAppointmentCountText
        {
            get => _totalAppointmentCountText;
            set
            {
                _totalAppointmentCountText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the total visit count text.
        /// </summary>
        /// <value>
        ///     The total visit count text.
        /// </value>
        public string TotalVisitCountText
        {
            get => _totalVisitCountText;
            set
            {
                _totalVisitCountText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the visit count text.
        /// </summary>
        /// <value>
        ///     The visit count text.
        /// </value>
        public string VisitCountText
        {
            get => _visitCountText;
            set
            {
                _visitCountText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the appointment count text.
        /// </summary>
        /// <value>
        ///     The appointment count text.
        /// </value>
        public string AppointmentCountText
        {
            get => _appointmentCountText;
            set
            {
                _appointmentCountText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the start date.
        /// </summary>
        /// <value>
        ///     The start date.
        /// </value>
        public DateTimeOffset StartDate
        {
            get => _startDate;
            set
            {
                _startDate = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the start date.
        /// </summary>
        /// <value>
        ///     The start date.
        /// </value>
        public DateTimeOffset EndDate
        {
            get => _endDate;
            set
            {
                _endDate = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Occurs when a property value changes.
        /// </summary>
        /// <returns></returns>
        public event PropertyChangedEventHandler PropertyChanged;

        private void UpdateTotalStats()
        {
            TotalPatientCountText = "Total Patient Count:\t" + _statsDal.GetPatientCount();
            TotalAppointmentCountText = "Total Appointment Count:\t" + _statsDal.GetAppointmentCount();
            TotalVisitCountText = "Total Visit Count:\t" + _statsDal.GetVisitCount();
        }

        public void GetTimeFrameCounts()
        {
            var start = StartDate.Date.ToShortDateString();
            var end = EndDate.Date.ToShortDateString();
            AppointmentCountText = "Appointment Count From " + start + " - " + end + ":\t" +
                                   _statsDal.GetAppointmentCountInTimeFrame(StartDate.Date, EndDate.Date);
            VisitCountText = "Visit Count From " + start + " - " + end + ":\t" +
                             _statsDal.GetVisitCountInTimeFrame(StartDate.Date, EndDate.Date);
        }

        /// <summary>
        ///     Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}