﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Popups;
using HealthCareSystem.DAL;
using HealthCareSystem.Model;

namespace HealthCareSystem.ViewModel
{
    public class VisitViewModel : INotifyPropertyChanged
    {
        private readonly RoutineCheckDal _checkDal;
        private readonly VisitDal _visitDal;

        private string _bodyTemp;

        private string _diastolicText;

        private string _heightText;

        private string _oxygenText;

        private string _pulseText;

        private string _systolicText;

        private string _weightText;


        /// <summary>
        ///     Initializes a new instance of the <see cref="VisitViewModel" /> class.
        /// </summary>
        public VisitViewModel()
        {
            _checkDal = new RoutineCheckDal();
            _visitDal = new VisitDal();
        }

        /// <summary>
        ///     Gets or sets the height text.
        /// </summary>
        /// <value>
        ///     The height text.
        /// </value>
        public string HeightText
        {
            get => _heightText;
            set
            {
                _heightText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the weight text.
        /// </summary>
        /// <value>
        ///     The weight text.
        /// </value>
        public string WeightText
        {
            get => _weightText;
            set
            {
                _weightText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the systolic text.
        /// </summary>
        /// <value>
        ///     The systolic text.
        /// </value>
        public string SystolicText
        {
            get => _systolicText;
            set
            {
                _systolicText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the diastolic text.
        /// </summary>
        /// <value>
        ///     The diastolic text.
        /// </value>
        public string DiastolicText
        {
            get => _diastolicText;
            set
            {
                _diastolicText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the pulse text.
        /// </summary>
        /// <value>
        ///     The pulse text.
        /// </value>
        public string PulseText
        {
            get => _pulseText;
            set
            {
                _pulseText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the oxygen text.
        /// </summary>
        /// <value>
        ///     The oxygen text.
        /// </value>
        public string OxygenText
        {
            get => _oxygenText;
            set
            {
                _oxygenText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the body temporary text.
        /// </summary>
        /// <value>
        ///     The body temporary text.
        /// </value>
        public string BodyTempText
        {
            get => _bodyTemp;
            set
            {
                _bodyTemp = value;
                OnPropertyChanged();
            }
        }

        public Visit Visit { get; set; }

        public string HeaderText { get; set; }

        /// <summary>
        ///     Occurs when a property value changes.
        /// </summary>
        /// <returns></returns>
        public event PropertyChangedEventHandler PropertyChanged;

        public async void UpdateRoutineCheck()
        {
            try
            {
                var check = _checkDal.GetRoutineCheckByVisit(Visit.Id);
                HeaderText = Visit.Appointment.Patient.PatientFullName;

                HeightText = Convert.ToString(check.Height);
                WeightText = Convert.ToString(check.Weight);
                SystolicText = Convert.ToString(check.BpSystolic);
                DiastolicText = Convert.ToString(check.BpDiastolic);
                OxygenText = Convert.ToString(check.OxygenSaturation);
                PulseText = Convert.ToString(check.PulseBpm);
                BodyTempText = Convert.ToString(check.BodyTemp);
            }
            catch (Exception e)
            {
                var msgBox = new MessageDialog(e.Message);
                msgBox.Content = "\nSomething went wrong updating the routine check.";
                msgBox.Title = "Something went wrong...";
                await msgBox.ShowAsync();
            }
        }

        /// <summary>
        ///     Enters the routine check click.
        /// </summary>
        public async void EnterRoutineCheckClick()
        {
            var bpSystolic = Convert.ToInt32(_systolicText);
            var bpDiastolic = Convert.ToInt32(_diastolicText);
            var oxygenSaturation = Convert.ToInt32(_oxygenText);
            var pulseBpm = Convert.ToInt32(_pulseText);
            var height = Convert.ToDecimal(_heightText);
            var weight = Convert.ToDecimal(_weightText);
            var bodyTemp = Convert.ToDecimal(_bodyTemp);
            try
            {
                var check = _checkDal.GetRoutineCheckByVisit(Visit.Id);

                if (check == null)
                {
                    _checkDal.AddRoutineCheck(new RoutineCheck(Visit.Id, bpSystolic, bpDiastolic, oxygenSaturation,
                        pulseBpm, height, weight, bodyTemp));
                    var msgBox = new MessageDialog("Routine Check Added");
                    msgBox.Content = "\nThe routine check has been successfully added to the visit.";
                    msgBox.Title = "Routine Check Added";
                    await msgBox.ShowAsync();
                }
                else
                {
                    _checkDal.UpdateRoutineCheck(new RoutineCheck(Visit.Id, bpSystolic, bpDiastolic, oxygenSaturation,
                        pulseBpm, height, weight, bodyTemp));
                    var msgBox = new MessageDialog("Routine Check Updated");
                    msgBox.Content = "\nThe routine check has been successfully updated.";
                    msgBox.Title = "Routine Check Updated";
                    await msgBox.ShowAsync();
                }
            }
            catch (Exception e)
            {
                var msgBox = new MessageDialog(e.Message);
                msgBox.Content = "\nSomething went wrong adding the routine check.";
                msgBox.Title = "Something went wrong...";
                await msgBox.ShowAsync();
            }
        }

        /// <summary>
        ///     Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}