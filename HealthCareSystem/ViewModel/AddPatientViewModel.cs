﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using HealthCareSystem.DAL;
using HealthCareSystem.Model;
using HealthCareSystem.Properties;
using HealthCareSystem.Utility;

namespace HealthCareSystem.ViewModel
{
    internal class AddPatientViewModel : INotifyPropertyChanged
    {
        public AddPatientViewModel()
        {
            Cache = new PatientCache();
            PatientBrowseList = new ObservableCollection<Patient>(Cache.Cache);
            StateList = StatesAndAbbreviations.GetStateNameList();
            DoctorList = new Dictionary<string, Doctor>();
            var dal = new DoctorDal();
            var doctorIds = dal.GetAllDoctors();
            _patientNames = new List<Patient>();
            _patientDb = new PatientDal();
            EditMode = true;
            AddMode = false;
            _searchProvider = new PatientSearches();
            SearchDate = new DateTimeOffset(DateTime.Today);
            SelectedPatientBirthDate = new DateTimeOffset(DateTime.Today);
            foreach (var doctor in doctorIds)
            {
                try
                {
                    var name = UserDal.GetNameFromId(doctor.UserId);
                    DoctorList.Add(name, doctor);
                }
                catch (Exception e)
                {
                    
                }
                
            }
        }

        public IDictionary<string, Doctor> DoctorList { get; set; }

        public bool AddMode
        {
            get => _addMode;
            set
            {
                _addMode = value;
                OnPropertyChanged();
            }
        }

        public bool EditMode
        {
            get => _editMode;
            set
            {
                _editMode = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public void AddPatient()
        {
            if (Validate())
            {
                var doctorId = DoctorList[SelectedPatientPrimaryDoctor].DoctorId;
                var state = StatesAndAbbreviations.GetAbbreviationFromStateName(SelectedState);
                var patient = new Patient(0, _ssn, _selectedPatientFName, _selectedPatientLName,
                    SelectedPatientSex, _selectedPatientBirthDate.DateTime, new Address(Address, City, state, Zipcode),
                    Phone, doctorId);
                new PatientDal().AddPatient(patient);
                Feedback = "Patient added successfully";
                ClearFields();
                PatientBrowseList.Add(patient);
            }
        }

        public void EditPatient()
        {
            if (Validate())
            {
                var doctorId = -1;
                var state = "";
                try
                {
                    doctorId = DoctorList[SelectedPatientPrimaryDoctor].DoctorId;
                }
                catch (Exception e)
                {
                    doctorId = SelectedPatient.PrimaryDoctor;
                }

                try
                {
                    state = StatesAndAbbreviations.GetAbbreviationFromStateName(SelectedState);
                }
                catch (Exception e)
                {
                    doctorId = SelectedPatient.PrimaryDoctor;
                }


                var patient = new Patient(SelectedPatientId, _ssn, _selectedPatientFName, _selectedPatientLName,
                    SelectedPatientSex, _selectedPatientBirthDate.DateTime,
                    new Address(SelectedPatient.Address.AddressId, Address, City, state, Zipcode), Phone,
                    doctorId);
                new PatientDal().EditPatient(patient);
                Feedback = "Patient Edited successfully";
                RefreshAllPatients();
            }
        }

        public void SearchPatient()
        {
            var patientSearch = new PatientSearch();
            if (SearchPatientLastName != null || SearchPatientLastName != "")
                patientSearch.LastName = SearchPatientLastName;
            if (SearchDate.Date != DateTime.Today) patientSearch.Birthdate = SearchDate.DateTime;
            if (SearchSsn != null || SearchSsn != "") patientSearch.Ssn = SearchSsn;
            var returnedPatients = new ObservableCollection<Patient>(_searchProvider.FindPatients(patientSearch));
            PatientBrowseList.Clear();
            foreach (var patient in returnedPatients) PatientBrowseList.Add(patient);
        }

        public void ClearSearch()
        {
            SearchPatientLastName = null;
            SearchDate = new DateTimeOffset(DateTime.Today);
            SearchSsn = null;
            RefreshAllPatients();
        }

        public void AddPatientmode()
        {
            ClearFields();
            AddMode = true;
            EditMode = false;
        }

        public void EditPatientmode()
        {
            ClearFields();
            AddMode = false;
            EditMode = true;
        }

        private bool Validate()
        {
            var isValid = false;
            if (IsValidSsn(_ssn))
            {
                if (IsValidPhone(_phone))
                {
                    if (IsValidZip(_zipcode))
                        isValid = true;
                    else
                        Feedback = "Please enter a valid zip code in the format of xxxxx";
                }
                else
                {
                    Feedback = "Please enter a phone number in the format of xxx-xxx-xxxx";
                }
            }
            else
            {
                Feedback = "Please enter a ssn in the format of xxx-xx-xxxx";
            }

            return isValid;
        }

        private void RefreshAllPatients()
        {
            Cache = new PatientCache();
            PatientBrowseList.Clear();
            foreach (var patient in Cache.Cache) PatientBrowseList.Add(patient);
        }

        private bool IsValidZip(string zip)
        {
            var isVaild = false;
            try
            {
                var regex = new Regex(@"^\d{5}(?:[-\s]\d{4})?$");
                return regex.IsMatch(zip);
            }
            catch (Exception e)
            {
            }

            return isVaild;
        }

        private bool IsValidPhone(string phone)
        {
            var isVaild = false;
            try
            {
                var regex = new Regex(@"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$");
                isVaild = regex.IsMatch(phone);
            }
            catch (Exception e)
            {
            }

            return isVaild;
        }

        private bool IsValidSsn(string ssn)
        {
            var isVaild = false;
            try
            {
                var regex = new Regex(@"\d{3}-\d{2}-\d{4}");
                return regex.IsMatch(ssn);
            }
            catch (Exception e)
            {
            }

            return isVaild;
        }

        private void ClearFields()
        {
            SelectedPatientFName = null;
            SelectedPatientLName = null;
            SelectedPatientSex = null;
            SelectedPatientPrimaryDoctor = null;
            SelectedPatientSex = null;
            Ssn = null;
            Address = null;
            SelectedState = null;
            SelectedPatientBirthDate = new DateTimeOffset(DateTime.Today);
            City = null;
            Zipcode = null;
            Phone = null;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region DataMembers

        private Patient _selectedPatient;
        private string _feedback;
        private int _selectedPatientId;
        private string _selectedPatientFName;
        private string _selectedPatientLName;
        private DateTimeOffset _selectedPatientBirthDate;
        private string _ssn;
        private string _phone;
        private string _selectedState;
        private string _city;
        private string _zipcode;
        public PatientCache Cache;
        private string _patientBoxText;
        private string _address;
        private List<Patient> _patientNames;
        private readonly PatientDal _patientDb;
        private string _searchPatientLastName;
        private ObservableCollection<Patient> _patientBrowseList;
        private bool _addMode;
        private bool _editMode;
        private readonly PatientSearches _searchProvider;
        private string _searchSsn;
        private DateTimeOffset _searchDate;

        #endregion

        #region Properties

        public string Feedback
        {
            get => _feedback;
            set
            {
                _feedback = value;
                OnPropertyChanged();
            }
        }


        public IList<string> StateList;


        public ObservableCollection<Patient> PatientBrowseList
        {
            get => _patientBrowseList;
            set
            {
                _patientBrowseList = value;
                OnPropertyChanged(nameof(PatientBrowseList));
            }
        }

        public Patient SelectedPatient
        {
            get => _selectedPatient;
            set
            {
                if (value == null)
                {
                    ClearFields();
                    _selectedPatient = value;
                }
                else
                {
                    SelectedPatientFName = value.FirstName;
                    SelectedPatientLName = value.LastName;
                    SelectedPatientId = value.PatientId;
                    SelectedPatientSex = value.Sex;
                    SelectedPatientBirthDate = value.Birthdate;
                    Ssn = value.Ssn;
                    Phone = value.PhoneNumber;
                    Address = value.Address.Streetaddress;
                    City = value.Address.City;
                    Zipcode = value.Address.Zipcode;
                    _selectedPatient = value;
                }
            }
        }


        public int SelectedPatientId
        {
            get => _selectedPatientId;
            set
            {
                _selectedPatientId = value;
                OnPropertyChanged();
            }
        }


        public string SelectedPatientFName
        {
            get => _selectedPatientFName;
            set
            {
                _selectedPatientFName = value;
                OnPropertyChanged();
            }
        }

        public string PatientBoxText
        {
            get => _patientBoxText;
            set
            {
                _patientBoxText = value;
                OnPropertyChanged();
            }
        }

        public string SelectedPatientLName
        {
            get => _selectedPatientLName;
            set
            {
                _selectedPatientLName = value;
                OnPropertyChanged();
            }
        }


        public string SelectedPatientSex { get; set; }


        public DateTimeOffset SelectedPatientBirthDate
        {
            get => _selectedPatientBirthDate;
            set
            {
                _selectedPatientBirthDate = value;
                OnPropertyChanged();
            }
        }

        public string SelectedPatientPrimaryDoctor { get; set; }

        public string[] Sex { get; } = {"M", "F"};

        public string Ssn
        {
            get => _ssn;
            set
            {
                _ssn = value;
                OnPropertyChanged();
            }
        }


        public string Phone
        {
            get => _phone;
            set
            {
                _phone = value;
                OnPropertyChanged();
            }
        }

        public string SelectedState
        {
            get => _selectedState;
            set
            {
                if (value == _selectedState) return;
                _selectedState = value;
                OnPropertyChanged();
            }
        }

        public string City
        {
            get => _city;
            set
            {
                if (value == _city) return;
                _city = value;
                OnPropertyChanged();
            }
        }

        public string Zipcode
        {
            get => _zipcode;
            set
            {
                if (value == _zipcode) return;
                _zipcode = value;
                OnPropertyChanged();
            }
        }

        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                OnPropertyChanged();
            }
        }

        public string SearchPatientLastName
        {
            get => _searchPatientLastName;
            set
            {
                _searchPatientLastName = value;
                OnPropertyChanged();
            }
        }

        public string SearchSsn
        {
            get => _searchSsn;
            set
            {
                _searchSsn = value;
                OnPropertyChanged();
            }
        }

        public DateTimeOffset SearchDate
        {
            get => _searchDate;
            set
            {
                _searchDate = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}