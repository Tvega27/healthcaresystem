﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using HealthCareSystem.DAL;
using HealthCareSystem.Model;
using HealthCareSystem.Properties;
using HealthCareSystem.View;

namespace HealthCareSystem.ViewModel
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private string _userName;


        public LoginViewModel()
        {
            UserName = "";
        }

        public string UserName

        {
            get => _userName;

            set

            {
                _userName = value;

                OnPropertyChanged();
            }
        }

        public string Password { private get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void AttemptLogin()
        {
            var admin = false;
            if (UserDal.CheckCredentials(UserName, Password))
            {
                if (UserDal.CheckAdmin(UserName)) admin = true;
                var frame = Window.Current.Content as Frame;
                frame.Navigate(typeof(MainPage), new LoginTicket(UserName, admin));
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}