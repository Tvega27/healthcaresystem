﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using HealthCareSystem.DAL;
using HealthCareSystem.Model;
using HealthCareSystem.View;

namespace HealthCareSystem.ViewModel
{
    public class VisitTestViewModel : INotifyPropertyChanged
    {
        private string _diagnosisText;

        private bool _enableFinalizeVisit;

        private bool _enableOrderTests;

        private bool _enableResults;

        private bool _enableRoutineCheck;

        private string _notesText;
        private readonly OrderedTestsDal _orderedTestsDal;

        private string _prognosisText;


        private string _resultsText;

        private OrderedTest _selectedTest;

        private Visit _selectedVisit;

        private bool _showFinalizedVisitIsCheck;

        private DateTimeOffset _testCompletedDate;

        private List<OrderedTest> _tests;
        private readonly VisitDal _visitDal;

        private List<Visit> _visits;

        /// <summary>
        ///     Initializes a new instance of the <see cref="VisitTestViewModel" /> class.
        /// </summary>
        public VisitTestViewModel()
        {
            _visitDal = new VisitDal();
            _orderedTestsDal = new OrderedTestsDal();
            TestCompletedDate = DateTime.Now;
            ShowFinalizedIsChecked = false;
            UpdateUnfinalizedVisitList();
        }

        /// <summary>
        ///     Gets or sets the selected visit.
        /// </summary>
        /// <value>
        ///     The selected visit.
        /// </value>
        public Visit SelectedVisit
        {
            get => _selectedVisit;
            set
            {
                _selectedVisit = value;
                OnPropertyChanged();
                UpdateOrderedTests();
                ResultsText = string.Empty;
                PrognosisText = _selectedVisit.Prognosis;
                NotesText = _selectedVisit.Notes;
                DiagnosisText = _selectedVisit.Diagnosis;

                if (DiagnosisText == null)
                {
                    EnableRoutineCheck = true;
                }
                else
                {
                    EnableRoutineCheck = false;
                    EnableFinalizeVisit = false;
                }

                EnableOrderTests = true;
                EnableResults = false;
            }
        }


        /// <summary>
        ///     Gets or sets the selected test.
        /// </summary>
        /// <value>
        ///     The selected test.
        /// </value>
        public OrderedTest SelectedTest
        {
            get => _selectedTest;
            set
            {
                _selectedTest = value;
                OnPropertyChanged();
                EnableResults = true;
                ResultsText = _selectedTest.Results;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether [enable results].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [enable results]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableResults
        {
            get => _enableResults;
            set
            {
                _enableResults = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether [enable routine check].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [enable routine check]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableRoutineCheck
        {
            get => _enableRoutineCheck;
            set
            {
                _enableRoutineCheck = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether [enable order tests].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [enable order tests]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableOrderTests
        {
            get => _enableOrderTests;
            set
            {
                _enableOrderTests = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether [enable finalize visit].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [enable finalize visit]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableFinalizeVisit
        {
            get => _enableFinalizeVisit;
            set
            {
                _enableFinalizeVisit = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        ///     Gets or sets the diagnosis text.
        /// </summary>
        /// <value>
        ///     The diagnosis text.
        /// </value>
        public string DiagnosisText
        {
            get => _diagnosisText;
            set
            {
                _diagnosisText = value;
                OnPropertyChanged();

                if (ShowFinalizedIsChecked == false) EnableFinalizeVisit = true;
            }
        }

        public bool ShowFinalizedIsChecked
        {
            get => _showFinalizedVisitIsCheck;
            set
            {
                _showFinalizedVisitIsCheck = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the prognosis text.
        /// </summary>
        /// <value>
        ///     The prognosis text.
        /// </value>
        public string PrognosisText
        {
            get => _prognosisText;
            set
            {
                _prognosisText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the visits.
        /// </summary>
        /// <value>
        ///     The visits.
        /// </value>
        public List<Visit> Visits
        {
            get => _visits;
            set
            {
                _visits = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the tests.
        /// </summary>
        /// <value>
        ///     The tests.
        /// </value>
        public List<OrderedTest> Tests
        {
            get => _tests;
            set
            {
                _tests = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the results text.
        /// </summary>
        /// <value>
        ///     The results text.
        /// </value>
        public string ResultsText
        {
            get => _resultsText;
            set
            {
                _resultsText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the notes text.
        /// </summary>
        /// <value>
        ///     The notes text.
        /// </value>
        public string NotesText
        {
            get => _notesText;
            set
            {
                _notesText = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the order date.
        /// </summary>
        /// <value>
        ///     The order date.
        /// </value>
        public DateTimeOffset TestCompletedDate
        {
            get => _testCompletedDate;
            set
            {
                _testCompletedDate = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Occurs when a property value changes.
        /// </summary>
        /// <returns></returns>
        public event PropertyChangedEventHandler PropertyChanged;

        public void UpdateUnfinalizedVisitList()
        {
            Visits = _visitDal.GetAllUnfinalizedVisits();
            EnableRoutineCheck = false;
            EnableFinalizeVisit = false;
        }

        public async void UpdateVisit()
        {
            try
            {
                SelectedVisit.Prognosis = PrognosisText;
                SelectedVisit.Notes = NotesText;
                _visitDal.UpdateVisit(SelectedVisit);
            }
            catch (Exception e)
            {
                var msgBox = new MessageDialog(e.Message);
                msgBox.Content = "\nSomething went wrong updating the visit.";
                msgBox.Title = "Something went wrong...";
                await msgBox.ShowAsync();
            }
        }

        /// <summary>
        ///     Updates the ordered tests.
        /// </summary>
        public void UpdateOrderedTests()
        {
            if (SelectedVisit != null) Tests = _orderedTestsDal.GetOrderedTestsByVisit(SelectedVisit.Id);
        }

        /// <summary>
        ///     Gets the pending tests.
        /// </summary>
        public void GetPendingTests()
        {
            Tests = Tests.Where(x => x.Results == null).ToList();
        }

        /// <summary>
        ///     Gets the finalized visits.
        /// </summary>
        public void GetFinalizedVisits()
        {
            Visits = _visitDal.GetAllFinalizedVisits();
            EnableRoutineCheck = false;
            EnableResults = false;
            EnableOrderTests = false;
            EnableFinalizeVisit = false;
        }

        /// <summary>
        ///     Click that navigates to the order test window.
        /// </summary>
        public async void OrderTestClick()
        {
            try
            {
                UpdateVisit();
                var frame = Window.Current.Content as Frame;
                frame.Navigate(typeof(OrderTest), SelectedVisit);
            }
            catch (Exception e)
            {
                var msgBox = new MessageDialog(e.Message);
                msgBox.Content = "\nSomething went wrong navigating to the next page.";
                msgBox.Title = "Something went wrong...";
                await msgBox.ShowAsync();
            }
        }

        /// <summary>
        ///     Click that navigates to the routine check window.
        /// </summary>
        public async void StartRoutineCheck()
        {
            try
            {
                UpdateVisit();
                var frame = Window.Current.Content as Frame;
                frame.Navigate(typeof(RoutineCheckView), SelectedVisit);
            }
            catch (Exception e)
            {
                var msgBox = new MessageDialog(e.Message);
                msgBox.Content = "\nSomething went wrong navigating to the next page.";
                msgBox.Title = "Navigation Failed";
                await msgBox.ShowAsync();
            }
        }

        /// <summary>
        ///     Enters the test results click.
        /// </summary>
        public async void EnterTestResultsClick()
        {
            if (ResultsText != null)
                try
                {
                    SelectedTest.Results = ResultsText;
                    SelectedTest.CompletedDate = TestCompletedDate.Date;
                    _orderedTestsDal.UpdateOrderedTest(SelectedTest);
                    var msgBox = new MessageDialog("Test Updated");
                    msgBox.Content = "\nThe test results were successfully updated";
                    msgBox.Title = "Update Successful";
                    await msgBox.ShowAsync();
                    GetPendingTests();
                }
                catch (Exception ex)
                {
                    var msgBox = new MessageDialog(ex.Message);
                    msgBox.Content = "\nSomething went wrong with updating the test result.";
                    msgBox.Title = "Update Failed";
                    await msgBox.ShowAsync();
                }
        }

        public async void EnterDiagnosisClick()
        {
            if (DiagnosisText != null)
                try
                {
                    SelectedVisit.Diagnosis = DiagnosisText;
                    _visitDal.UpdateVisitDiagnosis(SelectedVisit);
                    var msgBox = new MessageDialog("Visit Finalized");
                    msgBox.Content = "\nThe visit was successfully updated and finalized";
                    msgBox.Title = "Visit Finalized";
                    await msgBox.ShowAsync();
                    UpdateUnfinalizedVisitList();
                }
                catch (Exception ex)
                {
                    var msgBox = new MessageDialog(ex.Message);
                    msgBox.Content = "\nSomething went wrong with updating the visit.";
                    msgBox.Title = "Update Failed";
                    await msgBox.ShowAsync();
                }
        }

        /// <summary>
        ///     Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}