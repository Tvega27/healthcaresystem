﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using HealthCareSystem.Annotations;
using HealthCareSystem.DAL;
using Microsoft.Toolkit.Uwp.UI.Controls;

namespace HealthCareSystem.ViewModel
{
    public class AdminViewModel : INotifyPropertyChanged
    {
        private readonly AdminQueryDal _adminDal;
        private string _feedback;
        private string _input;
        private ObservableCollection<Expander> _resultList;
        private DataTable _results;

        public AdminViewModel()
        {
            _adminDal = new AdminQueryDal();
            ResultList = new ObservableCollection<Expander>();
        }

        public DataTable Results
        {
            get => _results;
            set
            {
                _results = value;
                OnPropertyChanged();
            }
        }

        public string Input
        {
            get => _input;
            set
            {
                _input = value;
                OnPropertyChanged();
            }
        }

        public string Feedback
        {
            get => _feedback;
            set
            {
                _feedback = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Expander> ResultList
        {
            get => _resultList;
            set
            {
                _resultList = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void BindTable(DataTable table, DataGrid grid)
        {
            if (table == null) return;
            grid.Columns.Clear();
            grid.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.Visible;

            for (var i = 0; i < table.Columns.Count; i++)
                grid.Columns.Add(new DataGridTextColumn
                {
                    Header = table.Columns[i].ColumnName,
                    Binding = new Binding {Path = new PropertyPath("[" + i + "]")}
                });

            var collection = new ObservableCollection<object>();
            for (var index = 0; index < table.Rows.Count; index++)
            {
                var row = table.Rows[index];
                collection.Add(row.ItemArray);
            }

            grid.ItemsSource = collection;
        }

        public void ButtonSelect_OnClick(object sender, RoutedEventArgs e)
        {
            var expander = new Expander();
            var resultgrid = new DataGrid();
            resultgrid.AutoGenerateColumns = false;
            expander.Header = Input;
            var result = _adminDal.ExecuteSelectStatement(Input);
            BindTable(result, resultgrid);
            expander.Content = resultgrid;
            ResultList.Add(expander);
        }

        public void ButtonOther_OnClick(object sender, RoutedEventArgs e)
        {
            var executed = _adminDal.NonSelectCommand(Input);
            Feedback = executed
                ? "Command executed successfully"
                : "Error executing command, please check your statement and try again.";
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}